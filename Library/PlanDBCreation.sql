create table [PublishingOffices] (
	Id int not null identity(1,1) primary key,
	[Name] nvarchar(300) not null
);

create table [Cities] (
	Id int not null identity(1,1) primary key,
	[Name] nvarchar(200) not null
);

create table [Countries] (
	Id int not null identity(1,1) primary key,
	[Name] nvarchar(200) not null
);

create table [CatalogItems] (
	Id int not null identity(1,1) primary key,
	[Type] tinyint not null
);

create table [Authors] (
	Id int not null identity(1,1) primary key,
	FirstName nvarchar(50) not null,
	LastName nvarchar(200) not null,
	[Description] nvarchar(2000)
);

create table [AuthorsCatalogItems] (
	Id int not null identity(1,1) primary key,
	AuthorId int not null,
	CatalogItemId int not null,
	foreign key (AuthorId) references Authors(Id),
	foreign key (CatalogItemId) references CatalogItems(Id) on delete cascade,
	constraint UQ_AuthorsCatalogItems unique (AuthorId, CatalogItemId)
);

create table [Books] (
	Id int not null primary key foreign key references [CatalogItems](Id) on delete cascade,
	Title nvarchar(300) not null,
	[PageCount] smallint not null,
	CityId int not null,
	PublishingYear int not null,
	ISBN nchar(19),
	PublishingOfficeId int not null,
	[Description] nvarchar(2000),
	foreign key (PublishingOfficeId) references PublishingOffices(Id),
	foreign key (CityId) references Cities(Id)
);

create table [Patents] (
	Id int not null primary key foreign key references [CatalogItems](Id) on delete cascade,
	Title nvarchar(300) not null,
	[PageCount] smallint not null,
	CountryId int not null,
	NumberRegistration int not null,
	PublicationDate date not null,
	ApplicationDate date not null,
	[Description] nvarchar(2000),
	foreign key (CountryId) references Countries(Id),
	constraint UQ_Patents unique (NumberRegistration, CountryId)
); 

create table [Newspapers] (
	Id int not null primary key foreign key references [CatalogItems](Id) on delete cascade,
	Title nvarchar(300) not null,
	[PageCount] smallint not null,
	CityId int not null,
	Number int,
	ISSN nchar(13),
	PublicationYear int not null,
	PublicationDate date  not null,
	PublishingOfficeId int not null,
	[Description] nvarchar(2000),
	foreign key (PublishingOfficeId) references PublishingOffices(Id),
	foreign key (CityId) references Cities(Id)
);

create table [Users] (
	Id int not null identity(1,1) primary key,
	FirstName nvarchar(50),
	LastName nvarchar(200),
	[Login] nvarchar(30) not null unique,
	[Password] nvarchar(128) not null,
	[Role] nvarchar(30) not null
);

create type dtIntEntity as table (
	EntityID int
);

go
create view CatalogItemsView as
select
	b.Id Id,
	concat(
		(
			select string_agg(substring(FirstName, 0, 2) + '. ' + LastName, ',')
			FROM Authors a inner join AuthorsCatalogItems ci on a.Id = ci.AuthorId
			where ci.CatalogItemId = b.Id
		),
		' - ',
		b.Title,
		' (',
		b.PublishingYear,
		')'
	) [Name],
	b.PageCount,
	b.ISBN PublicId
from Books b 
union all
select 
	p.Id,
	concat(
		'�',
		p.Title,
		N'� �� ',
		format(p.PublicationDate, 'dd.MM.yyyy')
	) [Name],
	p.PageCount,
	convert(varchar(10), p.NumberRegistration) PublicId
from Patents p
union all
select
	n.Id,
	concat(
		n.Title,
		' ',
		iif(
			n.Number is null,
			'',
			concat(N'�', n.Number, '/')
		),
		convert(varchar(10), n.PublicationYear)
	) [Name],
	n.PageCount,
	n.ISSN PublicId
from Newspapers n;
