using AutoMapper;
using Library.BLL;
using Library.BLL.Interfaces;
using Library.BLL.Validators;
using Library.DAL;
using Library.DAL.Interfaces;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using FluentValidation;
using WebLibrary.Helpers;
using Microsoft.AspNetCore.Authentication.Cookies;
using System.Threading.Tasks;
using WebLibrary.Middlewares;
using Microsoft.AspNetCore.Http;

namespace WebLibrary
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            var connectionString = Configuration.GetConnectionString("DefaultConnection");
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/Account/Login";
                    options.Events.OnRedirectToAccessDenied = context =>
                    {
                        context.Response.StatusCode = StatusCodes.Status403Forbidden;
                        return Task.CompletedTask;
                    };
                });
            services.AddAuthorization();

            var mappingConfig = new MapperConfiguration(mc =>
            {
                mc.AddProfile(new MappingProfile());
            });

            IMapper mapper = mappingConfig.CreateMapper();
            services.AddSingleton(mapper);

            services.AddSingleton(new DBOptions() { ConnectionString = connectionString });

            services.AddTransient<ICityService, CityService>();
            services.AddTransient<ICountryService, CountryService>();
            services.AddTransient<IPublishingOfficeService, PublishingOfficeService>();
            services.AddTransient<IAuthorService, AuthorService>();
            services.AddTransient<ICatalogService, CatalogService>();
            services.AddTransient<IBookService, BookService>();
            services.AddTransient<INewspaperService, NewspaperService>();
            services.AddTransient<IPatentService, PatentService>();
            services.AddTransient<IUserService, UserService>();

            services.AddTransient<ICityRepository, CityRepository>();
            services.AddTransient<ICountryRepository, CountryRepository>();
            services.AddTransient<IPublishingOfficeRepository, PublishingOfficeRepository>();
            services.AddTransient<IAuthorRepository, AuthorRepository>();
            services.AddTransient<ICatalogItemRepository, CatalogItemRepository>();
            services.AddTransient<IBookRepository, BookRepository>();
            services.AddTransient<INewspaperRepository, NewspaperRepository>();
            services.AddTransient<IPatentRepository, PatentRepository>();
            services.AddTransient<IUserRepository, UserRepository>();

            services.AddTransient<AuthorSelectItemsHelper>();

            services.AddValidatorsFromAssemblyContaining<BookValidator>();
            services.AddControllersWithViews();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseMiddleware<ExceptionHandlerMiddleware>();
            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "/{controller=Catalog}/{action=Index}/{id?}");
            });
        }
    }
}
