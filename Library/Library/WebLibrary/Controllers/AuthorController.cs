﻿using Library.BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Library.Entities;
using FluentValidation;
using WebLibrary.Models;
using System.Linq;
using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Authorization;

namespace WebLibrary.Controllers
{
    [Authorize(Roles = "admin")]
    public class AuthorController : Controller
    {
        private readonly ILogger<AuthorController> _logger;
        private readonly IAuthorService _authorService;
        public AuthorController(ILogger<AuthorController> logger, IAuthorService authorService)
        {
            _logger = logger;
            _authorService = authorService;
        }

        // GET: AuthorController/Create
        public ActionResult Create()
        {        
            return PartialView(new NewAuthor());
        }

        // POST: AuthorController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewAuthor model)
        {
            try
             {
                model.Id = _authorService.InsertAuthor(new Author
                {
                    FirstName = model.FirstName,
                    LastName = model.LastName,
                    Description = model.Description
                });
                return Created(string.Empty, model);
            }
            catch (ValidationException ex)
            {
                model.ExeptionMessage = ex.Errors.Select(x => x.ErrorMessage).ToList();
                return BadRequest(model.ExeptionMessage);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when creating a new author.");
                model.ExeptionMessage = new List<string>() { ex.Message };
                return StatusCode(StatusCodes.Status500InternalServerError, model.ExeptionMessage);
            }
        }
    }
}
