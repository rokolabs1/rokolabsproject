﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using WebLibrary.Helpers;
using WebLibrary.Models;
using WebLibrary.Models.PatentModels;

namespace WebLibrary.Controllers
{
    public class PatentController : Controller
    {
        private readonly ILogger<PatentController> _logger;
        private readonly IPatentService _patentService;
        private readonly AuthorSelectItemsHelper _authorHelper;
        private readonly IMapper _mapper;

        public PatentController(ILogger<PatentController> logger, IPatentService patentService, AuthorSelectItemsHelper authorHelper, IMapper mapper)
        {
            _logger = logger;
            _patentService = patentService;
            _authorHelper = authorHelper;
            _mapper = mapper;
        }
        // GET: PatentController
        public ActionResult Index()
        {
            var patents = _patentService.GetPatents().Select(patent => new PatentCatalog
            {
                Id = patent.Id,
                Title = patent.Title,
                Country = patent.Country.Name,
                PublicationDate = patent.PublicationDate,
                Authors = patent.Authors.Select(author => author.FirstName + ' ' + author.LastName).ToList()
            });

            return View(patents);
        }

        // GET: PatentController/Details/5
        public ActionResult Details(int id)
        {
            List<int> ids = new List<int>(id);
            ids.Add(id);
            Patent patent = _patentService.GetPatents(ids).FirstOrDefault();
            var patentCatalog = _mapper.Map<PatentCatalog>(patent);
            return View(patentCatalog);
        }

        // GET: PatentController/Create
        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            return View(new NewPatent { 
                AvailableAuthors = _authorHelper.GetAuthorSelectItems() 
            });
        }

        // POST: PatentController/Create
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewPatent model)
        { 
            try
            {
                _patentService.InsertPatent(new Patent
                {
                    Title = model.Title,
                    PageCount = model.PageCount,
                    Country = new Country { Name = model.Country},
                    NumberRegistration = model.NumberRegistration,
                    PublicationDate = model.PublicationDate,
                    ApplicationDate = model.ApplicationDate,
                    Authors = model.Authors.Select(x => new Author { Id = x }).ToList(),
                    Description = model.Description
                });
                return RedirectToAction(nameof(Index));
            }
            catch (ValidationException ex)
            {
                model.ExeptionMessage = ex.Errors.Select(x => x.ErrorMessage).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when creating a new patent.");
                model.ExeptionMessage = new List<string>() { ex.Message };
            }
            model.AvailableAuthors = _authorHelper.GetAuthorSelectItems();
                return View(model);
        }

        // GET: PatentController/Edit/5
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
            try
            {
                List<int> ids = new List<int>(id);
                ids.Add(id);
                Patent patent = _patentService.GetPatents(ids).FirstOrDefault();
                if (patent == null)
                {
                    return NotFound();
                }
                var newPatent = _mapper.Map<NewPatent>(patent);
                newPatent.AvailableAuthors = _authorHelper.GetAuthorSelectItems();
                return View(newPatent);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when editing a patent.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: PatentController/Edit/5
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, NewPatent model)
        {
            try
            {
                _patentService.UpdatePatent(new Patent
                {
                    Id = id,
                    Title = model.Title,
                    ApplicationDate = model.ApplicationDate,
                    PageCount = model.PageCount,
                    Description = model.Description,
                    Country = new Country {Name = model.Country },
                    NumberRegistration = model.NumberRegistration,
                    PublicationDate = model.PublicationDate,
                    Authors = model.Authors.Select(x => new Author { Id = x }).ToList()
                });
                return RedirectToAction(nameof(Index));
            }
            catch (ValidationException ex)
            {
                model.ExeptionMessage = ex.Errors.Select(x => x.ErrorMessage).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when editing a patent.");
                model.ExeptionMessage = new List<string>() { ex.Message };
            }
            model.AvailableAuthors = _authorHelper.GetAuthorSelectItems();
            return View(model);
        }

        // GET: PatentController/Delete/5
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int id)
        {
            try
            {
                List<int> ids = new List<int>(id);
                ids.Add(id);
                Patent patent = _patentService.GetPatents(ids).FirstOrDefault();
                if (patent == null)
                {
                    return NotFound();
                }
                var patentCatalog = _mapper.Map<PatentCatalog>(patent);
                return View(patentCatalog);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when deletion a book.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: PatentController/Delete/5
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, PatentCatalog model)
        {
            try
            {
                _patentService.DeletePatent(id);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when deletion a patent.");
                model.ExeptionMessage = new List<string>() { ex.Message };
                return View(model);
            }
        }
    }
}
