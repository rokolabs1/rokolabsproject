﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using WebLibrary.Models;
using WebLibrary.Models.NewspaperModels;

namespace WebLibrary.Controllers
{
    public class NewspaperController : Controller
    {
        private readonly ILogger<NewspaperController> _logger;
        private readonly INewspaperService _newspaperService;
        private readonly IMapper _mapper;

        public NewspaperController(ILogger<NewspaperController> logger, INewspaperService newspaperService, IMapper mapper)
        {
            _logger = logger;
            _newspaperService = newspaperService;
            _mapper = mapper;
        }
        // GET: NewspaperController
        public ActionResult Index()
        {
            var newspapers = _newspaperService.GetNewspapers().Select(newspaper => new NewspaperCatalog
            {
                Id = newspaper.Id,
                Title = newspaper.Title,
                City = newspaper.City.Name,
                PublishingOffice = newspaper.PublishingOffice.Name,
                Number = newspaper.Number,
                PublicationDate = newspaper.PublicationDate,
                PublicationYear = newspaper.PublicationYear
            }) ;

            return View(newspapers);
        }

        // GET: NewspaperController/Details/5
        public ActionResult Details(int id)
        {
            List<int> ids = new List<int>(id);
            ids.Add(id);
            Newspaper newspaper = _newspaperService.GetNewspapers(ids).FirstOrDefault();
            var newspaperCatalog = _mapper.Map<NewspaperCatalog>(newspaper);
            newspaperCatalog.NewspaperIssues = _mapper.Map<List<NewspaperIssue>>(_newspaperService.GetNewspaperIssues(id));
            return View(newspaperCatalog);
        }

        // GET: NewspaperController/Create
        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            return View(new NewNewspaper());
        }

        // POST: NewspaperController/Create
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewNewspaper model)
        {
            try
            {
                _newspaperService.InsertNewspaper(new Newspaper
                {
                    Title = model.Title,
                    Number = model.Number,
                    PageCount = model.PageCount,
                    City = new City { Name = model.City },
                    ISSN = model.ISSN,
                    PublicationYear = model.PublicationYear,
                    PublicationDate = model.PublicationDate,
                    PublishingOffice = new PublishingOffice { Name = model.PublishingOffice },
                    Description = model.Description
                }) ;
                return RedirectToAction(nameof(Index));
            }
            catch (ValidationException ex)
            {
                model.ExeptionMessage = ex.Errors.Select(x => x.ErrorMessage).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when creating a new newspaper.");
                model.ExeptionMessage = new List<string>() { ex.Message };
            }
            return View(model);
        }

        // GET: NewspaperController/Edit/5
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
            try
            {
                List<int> ids = new List<int>(id);
                ids.Add(id);
                Newspaper newspaper = _newspaperService.GetNewspapers(ids).FirstOrDefault();
                if (newspaper == null)
                {
                    return NotFound();
                }
                var newNewspaper = _mapper.Map<NewNewspaper>(newspaper);
                return View(newNewspaper);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when editing a newspaper.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: NewspaperController/Edit/5
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, NewNewspaper model)
        {
            try
            {
                Newspaper newspaper = _mapper.Map<Newspaper>(model);
                newspaper.Id = id;
                _newspaperService.UpdateNewspaper(newspaper);
                return RedirectToAction(nameof(Index));
            }
            catch (ValidationException ex)
            {
                model.ExeptionMessage = ex.Errors.Select(x => x.ErrorMessage).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when editing a newspaper.");
                model.ExeptionMessage = new List<string>() { ex.Message };
            }
            return View(model);
        }

        // GET: NewspaperController/Delete/5
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int id)
        {
            try
            {
                List<int> ids = new List<int>(id);
                ids.Add(id);
                Newspaper newspaper = _newspaperService.GetNewspapers(ids).FirstOrDefault();
                if (newspaper == null)
                {
                    return NotFound();
                }
                var newspaperCatalog = _mapper.Map<NewspaperCatalog>(newspaper);
                return View(newspaperCatalog);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when deletion a newspaper.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: NewspaperController/Delete/5
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, NewspaperCatalog model)
        {
            try
            {
                _newspaperService.DeleteNewspaper(id);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when deletion a newspaper.");
                model.ExeptionMessage = new List<string>() { ex.Message };
                return View(model);
            }
        }
    }
}
