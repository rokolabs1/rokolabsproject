﻿using System;
using Library.BLL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Library.Entities;
using System.Linq;
using FluentValidation;
using System.Collections.Generic;
using WebLibrary.Models.BookModels;
using WebLibrary.Models;
using WebLibrary.Helpers;
using AutoMapper;
using Microsoft.AspNetCore.Authorization;

namespace WebLibrary.Controllers
{
    public class BookController : Controller
    {
        private readonly ILogger<BookController> _logger;
        private readonly IBookService _bookService;
        private readonly AuthorSelectItemsHelper _authorHelper;
        private readonly IMapper _mapper;

        public BookController(ILogger<BookController> logger, IBookService bookService, AuthorSelectItemsHelper authorHelper, IMapper mapper)
        {
            _logger = logger;
            _bookService = bookService;
            _authorHelper = authorHelper;
            _mapper = mapper;
        }
        // GET: BookController
        public ActionResult Index()
        {
            var books = _bookService.GetBooks().Select(book => new BookCatalog
            {
                Id = book.Id,
                Title = book.Title,
                City = book.City.Name,
                PublishingOffice = book.PublishingOffice.Name,
                PublishingYear = book.PublishingYear,
                Authors = book.Authors.Select(author => author.FirstName + ' ' + author.LastName).ToList()
            });

            return View(books);
        }

        // GET: BookController/Details/5
        public ActionResult Details(int id)
        {
            List<int> ids = new List<int>(id);
            ids.Add(id);
            Book book = _bookService.GetBooks(ids).FirstOrDefault();
            var bookCatalog = _mapper.Map<BookCatalog>(book);
            return View(bookCatalog);
        }

        // GET: BookController/Create
        [Authorize(Roles = "admin")]
        public ActionResult Create()
        {
            Content(User.Identity.Name);
            return View(new NewBook
            {
                AvailableAuthors = _authorHelper.GetAuthorSelectItems()
            });
        }

        // POST: BookController/Create
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(NewBook model)
        {
            try
            {
                _bookService.InsertBook(new Book
                {
                    Title = model.Title,
                    ISBN = model.ISBN,
                    PageCount = model.PageCount,
                    Description = model.Description,
                    PublishingYear = model.PublishingYear,
                    City = new City { Name = model.City },
                    PublishingOffice = new PublishingOffice { Name = model.PublishingOffice },
                    Authors = model.Authors.Select(x => new Author { Id = x }).ToList()
                });
                return RedirectToAction(nameof(Index));
            }
            catch (ValidationException ex)
            {
                model.ExeptionMessage = ex.Errors.Select(x => x.ErrorMessage).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when creating a new book.");
                model.ExeptionMessage = new List<string>() { ex.Message };
            }
            model.AvailableAuthors = _authorHelper.GetAuthorSelectItems();
            return View(model);
        }

        // GET: BookController/Edit/5
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
            try
            {
                List<int> ids = new List<int>(id);
                ids.Add(id);
                Book book = _bookService.GetBooks(ids).FirstOrDefault();
                if (book == null)
                {
                    return NotFound();
                }
                var newBook = _mapper.Map<NewBook>(book);
                newBook.AvailableAuthors = _authorHelper.GetAuthorSelectItems();
                return View(newBook);
            }
            catch(Exception ex)
            {
                _logger.LogError(ex, "Error when editing a book.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }

        }

        // POST: BookController/Edit/5
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, NewBook model)
        {
            try
            {
                _bookService.UpdateBook(new Book
                {
                    Id = id,
                    Title = model.Title,
                    ISBN = model.ISBN,
                    PageCount = model.PageCount,
                    Description = model.Description,
                    PublishingYear = model.PublishingYear,
                    City = new City { Name = model.City },
                    PublishingOffice = new PublishingOffice { Name = model.PublishingOffice },
                    Authors = model.Authors.Select(x => new Author { Id = x }).ToList()
                });
                return RedirectToAction(nameof(Index));
            }
            catch (ValidationException ex)
            {
                model.ExeptionMessage = ex.Errors.Select(x => x.ErrorMessage).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when editing a book.");
                model.ExeptionMessage = new List<string>() { ex.Message };
            }
            model.AvailableAuthors = _authorHelper.GetAuthorSelectItems();
            return View(model);
        }

        // GET: BookController/Delete/5
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int id)
        {
            try
            {
                List<int> ids = new List<int>(id);
                ids.Add(id);
                Book book = _bookService.GetBooks(ids).FirstOrDefault();
                if(book == null)
                {
                    return NotFound();
                }
                var bookCatalog = _mapper.Map<BookCatalog>(book);
                return View(bookCatalog);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when deletion a book.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: BookController/Delete/5
        [Authorize(Roles = "admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, BookCatalog model)
        {
            try
            {
                _bookService.DeleteBook(id);
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when deletion a book.");
                model.ExeptionMessage = new List<string>() { ex.Message };
                return View(model);
            }
        }
    }
}
