﻿using AutoMapper;
using Library.BLL.Interfaces;
using Library.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using WebLibrary.Models.UserModels;

namespace WebLibrary.Controllers
{
    [Authorize(Roles = "admin")]
    public class UserController : Controller
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(ILogger<UserController> logger, IUserService userService, IMapper mapper)
        {
            _logger = logger;
            _userService = userService;
            _mapper = mapper;
        }

        // GET: UserController
        public ActionResult Index()
        {
            var users = _mapper.Map<List<UserCatalog>>(_userService.GetUsers());
            return View(users);
        }
        // GET: UserController/Edit/5
        public ActionResult Edit(int id)
        {
            try
            {
                User user = _userService.GetUserById(id);
                if (user == null)
                {
                    return NotFound();
                }
                var userCatalog = _mapper.Map<UserCatalog>(user);
                return View(userCatalog);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when editing a user role.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        // POST: UserController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id, UserCatalog model)
        {
            try
            {
                _userService.ChangeUserRole(_mapper.Map<User>(model));
                return RedirectToAction(nameof(Index));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when editing a user role.");
                model.ExeptionMessage = new List<string>() { ex.Message };
                return View(model);
            }
        }
    }
}
