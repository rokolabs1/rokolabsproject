﻿using AutoMapper;
using Library.BLL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using WebLibrary.Models.CatalogModels;

namespace WebLibrary.Controllers
{
    public class CatalogController : Controller
    {
        private readonly ILogger<CatalogController> _logger;
        private readonly ICatalogService _catalogService;
        private readonly IMapper _mapper;

        public CatalogController(ILogger<CatalogController> logger, ICatalogService catalogService, IMapper mapper)
        {
            _logger = logger;
            _catalogService = catalogService;
            _mapper = mapper;
        }
        // GET: CatalogController
        public ActionResult Index(int page = 1)
        {
            List<ItemCatalog> items = _mapper.Map<List<ItemCatalog>>(
                _catalogService.GetCatalogItems((page - 1) * Constants.PageSize, Constants.PageSize));

            var count = _catalogService.GetCatalogItemsCount();
            PaginatedModel<ItemCatalog> indexModel = new PaginatedModel<ItemCatalog>(count, page, Constants.PageSize)
            {
                Items = items
            };

            return View(indexModel);
        }

        // GET: CatalogController/Details/5
        public ActionResult Details(int id)
        {
            return RedirectToController("Details", id);
        }

        // GET: CatalogController/Edit/5
        [Authorize(Roles = "admin")]
        public ActionResult Edit(int id)
        {
            return RedirectToController("Edit", id);
        }


        // GET: CatalogController/Delete/5
        [Authorize(Roles = "admin")]
        public ActionResult Delete(int id)
        {
            return RedirectToController("Delete", id);
        }

        private ActionResult RedirectToController(string actionName, int id)
        {
            switch (_catalogService.GetTypeById(id))
            {
                case Library.DAL.CatalogItemType.Book:
                    return RedirectToAction(actionName, "Book", new { id = id });
                case Library.DAL.CatalogItemType.Newspaper:
                    return RedirectToAction(actionName, "Newspaper", new { id = id });
                case Library.DAL.CatalogItemType.Patent:
                    return RedirectToAction(actionName, "Patent", new { id = id });
                default:
                    return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
