﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Security.Claims;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Logging;
using WebLibrary.Models.AuthenticationModels;
using Library.BLL.Interfaces;
using AutoMapper;
using Library.Entities;
using System;
using FluentValidation;
using System.Linq;
using Microsoft.AspNetCore.Http;
using WebLibrary.Middlewares;

namespace WebLibrary.Controllers
{
    public class AccountController : Controller
    {
        private readonly ILogger<AccountController> _logger;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public AccountController(ILogger<AccountController> logger, IUserService userService, IMapper mapper)
        {
            _logger = logger;
            _userService = userService;
            _mapper = mapper;
        }
        [HttpGet]

        public IActionResult Login(string returnURL = "/")
        {
            TempData["ReturnURL"] = returnURL;
            return View(new SignIn());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Login(SignIn model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            try
            {
                User user = _userService.Login(_mapper.Map<User>(model));
                if (user == null)
                {
                    model.ExeptionMessage = new List<string>() { "Invalid username and/or password." };
                }
                else
                {
                    await Authenticate(user);
                    return Redirect(TempData["ReturnURL"].ToString());
                }

            }
            catch (ValidationException ex)
            {
                model.ExeptionMessage = ex.Errors.Select(x => x.ErrorMessage).ToList();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when sign in.");
                model.ExeptionMessage = new List<string>() { ex.Message };
            }
            return View(model);
        }
        [HttpGet]

        public IActionResult Register()
        {
            return View(new Registration());
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Register(Registration model)
        {
            try
            {
                User user = _userService.Register(_mapper.Map<User>(model));
                _logger.LogInformation("@{LogEntity}", new LogEntity
                {
                    Message = "User is registered",
                    UserName = model.Login,
                    MethodInfo = nameof(Register),
                    ClassName = GetType().FullName,
                });
                await Authenticate(user);
                return Redirect(TempData["ReturnURL"].ToString());
            }
            catch (ValidationException ex)
            {
                model.ExeptionMessage = ex.Errors.Select(x => x.ErrorMessage).ToList();
            }
            catch (Exception ex)
            {

                _logger.LogError(ex, "Error when sign up.");
                model.ExeptionMessage = new List<string>() { ex.Message };
            }
            return View(model);
        }

        private async Task Authenticate(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimsIdentity.DefaultNameClaimType, user.Login),
                new Claim(ClaimsIdentity.DefaultRoleClaimType, user.Role),
            };
            ClaimsIdentity id = new ClaimsIdentity(claims, "ApplicationCookie", ClaimsIdentity.DefaultNameClaimType, ClaimsIdentity.DefaultRoleClaimType);
            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(id));

            _logger.LogInformation("{@LogEntity}", new LogEntity
            {
                Message = "Used is logged in.",
                UserName = User.Identity.Name,
                MethodInfo = nameof(Authenticate),
                ClassName = GetType().FullName,
            });
        }

        public async Task<IActionResult> Logout()
        {
            LogEntity entity = new LogEntity
            {
                Message = "Used is logged out.",
                UserName = User.Identity.Name,
                MethodInfo = nameof(Logout),
                ClassName = GetType().FullName,
            };
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            _logger.LogInformation("{@LogEntity}", entity);
            return Redirect(Request.Headers["Referer"].ToString());
        }
    }
}
