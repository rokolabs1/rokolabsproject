﻿using Library.BLL.Interfaces;
using Microsoft.AspNetCore.Mvc.Rendering;
using System.Collections.Generic;
using System.Linq;

namespace WebLibrary.Helpers
{
    public class AuthorSelectItemsHelper
    {
        private readonly IAuthorService _authorService;

        public AuthorSelectItemsHelper(IAuthorService authorService)
        {
            _authorService = authorService;
        }

        public List<SelectListItem> GetAuthorSelectItems()
        {
            return _authorService.GetAuthors().
                Select(a => new SelectListItem
                {
                    Value = a.Id.ToString(),
                    Text = a.FirstName + ' ' + a.LastName
                }).ToList();
        }
    }
}
