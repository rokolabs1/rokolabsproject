﻿using System;
using System.Collections.Generic;

namespace WebLibrary.Models.CatalogModels
{
    public class PageModel
    {
        private const int PagesAroundCurrent = 3;
        private readonly int selectedPageNumber;

        public List<PageButton> Buttons { get; set; }

        
        public PageModel(int count, int pageNumber, int pageSize)
        {
            selectedPageNumber = pageNumber;

            int totalPages = (int)Math.Ceiling(count / (double)pageSize);

            Buttons = new List<PageButton>();

            Buttons.Add(CreatePageButton(1));

            if (selectedPageNumber > PagesAroundCurrent + 2)
            {
                Buttons.Add(CreateEmptyButon());
            }

            int from = pageNumber - PagesAroundCurrent;
            if (from <= 1)
            {
                from = 2;
            }
            int to = pageNumber + PagesAroundCurrent;
            if (to >= totalPages)
            {
                to = totalPages;
            }
            
            for (int i = from; i <= to; i++)
            {
                Buttons.Add(CreatePageButton(i));
            }

            if (to < totalPages - 1)
            {
                Buttons.Add(CreateEmptyButon());
            }

            if (to < totalPages)
            {
                Buttons.Add(CreatePageButton(totalPages));
            }
        }

        private PageButton CreatePageButton(int pageNumber)
        {
            return new PageButton()
            {
                PageNumber = pageNumber,
                IsClickable = pageNumber != selectedPageNumber
            };
        }

        private PageButton CreateEmptyButon()
        {
            return new PageButton();
        }
    }
}

