﻿namespace WebLibrary.Models.CatalogModels
{
    public class PageButton
    {
        public int? PageNumber { get; set; }
        public bool IsClickable { get; set; }
    }
}

