﻿namespace WebLibrary.Models.CatalogModels
{
    public class ItemCatalog
    {
        public int CatalogItemId { get; set; }
        public string Name { get; set; }
        public string PublicId { get; set; }
        public int PageCount { get; set; }
    }
}
