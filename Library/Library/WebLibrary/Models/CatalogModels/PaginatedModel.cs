﻿using System.Collections.Generic;

namespace WebLibrary.Models.CatalogModels
{
    public class PaginatedModel<T>
    {
        public IEnumerable<T> Items { get; set; }
        public PageModel Paging { get; }

        public PaginatedModel(int count, int pageNumber, int pageSize)
        {
            Paging = new PageModel(count, pageNumber, pageSize);
        }
    }
}
