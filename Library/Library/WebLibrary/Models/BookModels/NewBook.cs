﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebLibrary.Models
{
    public class NewBook
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public short PageCount { get; set; }
        [Required]
        public string City { get; set; }
        [Required]
        public int PublishingYear { get; set; } = DateTime.Today.Year;
        public string ISBN { get; set; }
        [Required]
        public string PublishingOffice { get; set; }
        public string Description { get; set; }
        [Required]
        public List<int> Authors { get; set; }
        public List<SelectListItem> AvailableAuthors { get; set; }
        public List <string> ExeptionMessage { get; set; }
    }
}
