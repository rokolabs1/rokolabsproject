﻿using System.Collections.Generic;

namespace WebLibrary.Models.BookModels
{
    public class BookCatalog
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string City { get; set; }
        public string ISBN { get; set; }
        public int PageCount { get; set; }
        public int PublishingYear { get; set; }
        public string PublishingOffice { get; set; }
        public List<string> Authors { get; set; }
        public string Description { get; set; }
        public List<string> ExeptionMessage { get; set; }
    }
}
