﻿using System.Collections.Generic;

namespace WebLibrary.Models.UserModels
{
    public class UserCatalog
    {
        public int Id { get; set; }
        public string Role { get; set; }
        public string Login { get; set; }
        public List<string> ExeptionMessage { get; set; }
    }
}
