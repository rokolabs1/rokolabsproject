﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebLibrary.Models
{
    public class NewAuthor
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Description { get; set; }
        public List<string> ExeptionMessage { get; set; }
    }
}
