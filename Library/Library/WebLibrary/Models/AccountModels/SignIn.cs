﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebLibrary.Models.AuthenticationModels
{
    public class SignIn
    {
        [Required]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [MinLength(3)]
        public string Password { get; set; }
        public List<string> ExeptionMessage { get; set; }
    }
}
