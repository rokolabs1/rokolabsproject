﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebLibrary.Models.AuthenticationModels
{
    public class Registration
    {
        [Required]
        public string Login { get; set; }

        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Compare("Password")]
        public string ConfirmPassword { get; set; }
        public List<string> ExeptionMessage { get; set; }
    }
}
