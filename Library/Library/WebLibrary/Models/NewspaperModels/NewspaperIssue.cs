﻿namespace WebLibrary.Models.NewspaperModels
{
    public class NewspaperIssue
    {
        public int Id { get; set; }
        public int Number { get; set; }
    }
}