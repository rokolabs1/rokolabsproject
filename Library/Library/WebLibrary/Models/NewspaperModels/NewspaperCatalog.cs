﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebLibrary.Models.NewspaperModels
{
    public class NewspaperCatalog
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? Number { get; set; }
        public short PageCount { get; set; }
        public string City { get; set; }
        public string ISSN { get; set; }
        public int PublicationYear { get; set; }

        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime PublicationDate { get; set; }
        public string PublishingOffice { get; set; }
        public string Description { get; set; }
        public List<NewspaperIssue> NewspaperIssues { get; set; }
        public List<string> ExeptionMessage { get; set; }
    }
}
