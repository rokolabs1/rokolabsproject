﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebLibrary.Models
{
    public class NewNewspaper
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public int Number { get; set; }
        [Required]
        public short PageCount { get; set; }
        [Required]
        public string City { get; set; }
        public string ISSN { get; set; }
        [Required]
        public int PublicationYear { get; set; } = DateTime.Today.Year;

        [Required]
        [DataType(DataType.Date)]
        public DateTime PublicationDate { get; set; } = DateTime.Today;

        [Required]
        public string PublishingOffice { get; set; }
        public string Description { get; set; }
        public List<string> ExeptionMessage { get; set; }
    }
}
