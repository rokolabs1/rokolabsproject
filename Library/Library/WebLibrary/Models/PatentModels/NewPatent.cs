﻿using Microsoft.AspNetCore.Mvc.Rendering;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebLibrary.Models
{
    public class NewPatent
    {
        [Required]
        public string Title { get; set; }
        [Required]
        public short PageCount { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public int NumberRegistration { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime PublicationDate { get; set; } = DateTime.Today;
        [Required]
        [DataType(DataType.Date)]
        public DateTime ApplicationDate { get; set; } = DateTime.Today;
        [Required]
        public List<int> Authors { get; set; }
        public List<SelectListItem> AvailableAuthors { get; set; } 
        public string Description { get; set; }
        public List<string> ExeptionMessage { get; set; }
    }
}
