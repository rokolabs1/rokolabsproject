﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebLibrary.Models.PatentModels
{
    public class PatentCatalog
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public string Country { get; set; }
        [DisplayFormat(DataFormatString = "{0:dd.MM.yyyy}")]
        public DateTime PublicationDate { get; set; }
        public List<string> Authors { get; set; }
        public List<string> ExeptionMessage { get; set; }
    }
}
