﻿function initializeAuthorSelect2(getModalViewUrl) {
    var onAuthorCreated = function (data) {
        var dataOption = {
            id: data.id,
            text: data.firstName + ' ' + data.lastName,
        };

        var newOption = new Option(dataOption.text, dataOption.id, false, true);
        $('.js-example-basic-multiple').append(newOption).trigger('change');
        $('#modDialog').modal('hide');
    };

    var onAuthorCreateFailed = function (data) {
        for (var i = 0; i < data.responseJSON.length; i++) {
            $('#error-author').append(`<h6 class="text-danger">${data.responseJSON[i]}</h6>`);
        }
    };

    var onSubmitAuthor = function (e) {
        $('#error-author').empty();
        if ($('#authorCreateForm').valid()) {
            $.ajax({
                type: 'post',
                url: e.target.action,
                data: $('#authorCreateForm').serialize(),
                error: onAuthorCreateFailed,
                success: onAuthorCreated,
            });
        }
        e.preventDefault();
    }

    var onCreateAuthorFormReceived = function (data) {
        $('#dialogContent').html(data);

        $('#authorCreateForm').on('submit', onSubmitAuthor);

        $.validator.unobtrusive.parse('#authorCreateForm');

        $('#modDialog').modal('show');
    }

    var onCreateAuthorClick = function (e) {
        $.get(getModalViewUrl, onCreateAuthorFormReceived);
    };

    var onDocumentReady = function () {
        $('.js-example-basic-multiple').select2();

        $.ajaxSetup({ cache: false });
        $("#createAuthor").click(onCreateAuthorClick);
    };

    $(document).ready(onDocumentReady);
}