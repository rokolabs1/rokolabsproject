﻿using AutoMapper;
using Library.DAL.DTO;
using Library.Entities;
using System.Linq;
using WebLibrary.Models;
using WebLibrary.Models.AuthenticationModels;
using WebLibrary.Models.BookModels;
using WebLibrary.Models.CatalogModels;
using WebLibrary.Models.NewspaperModels;
using WebLibrary.Models.PatentModels;
using WebLibrary.Models.UserModels;

namespace WebLibrary
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<City, CityDTO>();
            CreateMap<CityDTO, City>();

            CreateMap<Country, CountryDTO>();
            CreateMap<CountryDTO, Country>();

            CreateMap<PublishingOffice, PublishingOfficeDTO>();
            CreateMap<PublishingOfficeDTO, PublishingOffice>();

            CreateMap<Author, AuthorDTO>();
            CreateMap<AuthorDTO, Author>();

            CreateMap<Book, BookDTO>()
                .ForMember(obj => obj.CityId, opt => opt.MapFrom(obj => obj.City.Id))
                .ForMember(obj => obj.PublishingOfficeId, opt => opt.MapFrom(obj => obj.PublishingOffice.Id))
                .ForMember(obj => obj.AuthorIds, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.Id).ToList()));
            CreateMap<BookDTO, Book>();

            CreateMap<Patent, PatentDTO>()
                .ForMember(obj => obj.CountryId, opt => opt.MapFrom(obj => obj.Country.Id))
                .ForMember(obj => obj.AuthorIds, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.Id).ToList()));
            CreateMap<PatentDTO, Patent>()
                .ForPath(obj => obj.Country.Id, opt => opt.MapFrom(obj => obj.CountryId))
                .ForMember(obj => obj.Authors, opt => opt.MapFrom(obj => obj.AuthorIds.Select(a => new Author { Id = a })));

            CreateMap<Newspaper, NewspaperDTO>()
                .ForMember(obj => obj.CityId, opt => opt.MapFrom(obj => obj.City.Id))
                .ForMember(obj => obj.PublishingOfficeId, opt => opt.MapFrom(obj => obj.PublishingOffice.Id));
            CreateMap<NewspaperDTO, Newspaper>()
                .ForPath(obj => obj.City.Id, opt => opt.MapFrom(obj => obj.CityId))
                .ForPath(obj => obj.PublishingOffice.Id, opt => opt.MapFrom(obj => obj.PublishingOfficeId));

            CreateMap<Book, NewBook>()
                .ForMember(obj => obj.PublishingOffice, opt => opt.MapFrom(obj => obj.PublishingOffice.Name))
                .ForMember(obj => obj.City, opt => opt.MapFrom(obj => obj.City.Name))
                .ForMember(obj => obj.Authors, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.Id).ToList()));

            CreateMap<Patent, NewPatent>()
                .ForMember(obj => obj.Country, opt => opt.MapFrom(obj => obj.Country.Name))
                .ForMember(obj => obj.Authors, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.Id).ToList()));

            CreateMap<Newspaper, NewNewspaper>()
                .ForMember(obj => obj.PublishingOffice, opt => opt.MapFrom(obj => obj.PublishingOffice.Name))
                .ForMember(obj => obj.City, opt => opt.MapFrom(obj => obj.City.Name));
            CreateMap<NewNewspaper, Newspaper>()
               .ForMember(obj => obj.PublishingOffice, opt => opt.MapFrom(obj => new PublishingOffice { Name = obj.PublishingOffice }))
               .ForMember(obj => obj.City, opt => opt.MapFrom(obj => new City { Name = obj.City }));

            CreateMap<Book, BookCatalog>()
                .ForMember(obj => obj.PublishingOffice, opt => opt.MapFrom(obj => obj.PublishingOffice.Name))
                .ForMember(obj => obj.City, opt => opt.MapFrom(obj => obj.City.Name))
                .ForMember(obj => obj.Authors, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.FirstName + ' ' + a.LastName).ToList()));

            CreateMap<Patent, PatentCatalog>()
                .ForMember(obj => obj.Country, opt => opt.MapFrom(obj => obj.Country.Name))
                .ForMember(obj => obj.Authors, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.FirstName + ' ' + a.LastName).ToList()));

            CreateMap<CatalogItem, ItemCatalog>();

            CreateMap<Newspaper, NewspaperIssue>();

            CreateMap<Newspaper, NewspaperCatalog>()
                .ForMember(obj => obj.PublishingOffice, opt => opt.MapFrom(obj => obj.PublishingOffice.Name))
                .ForMember(obj => obj.City, opt => opt.MapFrom(obj => obj.City.Name));

            CreateMap<SignIn, User>();

            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();

            CreateMap<Registration, User>();

            CreateMap<User, UserCatalog>();
            CreateMap<UserCatalog, User>();

            CreateMap<CatalogItemViewDTO, CatalogItem>();
        }
    }
}
