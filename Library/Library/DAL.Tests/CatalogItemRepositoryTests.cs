﻿using NUnit.Framework;

namespace Library.DAL.Tests
{
    [TestFixture]
    public class CatalogItemRepositoryTests : BaseRepositoryTests<CatalogItemRepository>
    {
        [Test]
        public void InsertAndGetTest()
        {
            var repository = new CatalogItemRepository(GetLoggerMock().Object, GetDBOptions());

            var item = new CatalogItemDTO
            {
                Type = CatalogItemType.Book,
            };
            var id = repository.Insert(item);
            Assert.AreNotEqual(0, id);

            var saved = repository.Get(id);
            Assert.IsNotNull(saved);
            Assert.AreEqual(item.Type, saved.Type);
        }
    }
}
