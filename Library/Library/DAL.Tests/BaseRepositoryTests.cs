﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Moq;
using System.IO;

namespace Library.DAL.Tests
{
    public abstract class BaseRepositoryTests<T>
    {
        private static readonly string _connectionString;

        static BaseRepositoryTests()
        {
            var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false);

            IConfiguration config = builder.Build();

            _connectionString = config.GetConnectionString("DefaultConnection");
        }

        protected DBOptions GetDBOptions() => new DBOptions { ConnectionString = _connectionString };
        protected Mock<ILogger<T>> GetLoggerMock() => new Mock<ILogger<T>>();
    }
}
