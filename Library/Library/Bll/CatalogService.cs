﻿using Microsoft.Extensions.Logging;
using Library.DAL;
using Library.BLL.Interfaces;
using Library.DAL.Interfaces;
using System.Collections.Generic;
using Library.Entities;
using AutoMapper;

namespace Library.BLL
{
    public class CatalogService: ICatalogService
    {
        private readonly ILogger<CatalogService> _logger;
        private readonly ICatalogItemRepository _catalogItemRepository;
        private readonly IBookService _bookService;
        private readonly INewspaperService _newspaperService;
        private readonly IPatentService _patentService;
        private readonly IMapper _mapper;
        public CatalogService(
            ILogger<CatalogService> logger,
            ICatalogItemRepository catalogItemRepository,
            IBookService bookService,
            INewspaperService newspaperService,
            IPatentService patentService,
            IMapper mapper)
        {
            _logger = logger;
            _catalogItemRepository = catalogItemRepository;
            _bookService = bookService;
            _newspaperService = newspaperService;
            _patentService = patentService;
            _mapper = mapper;
        }
        public void DeleteItem(int globalId)
        {
            CatalogItemDTO item = _catalogItemRepository.Get(globalId);
            _catalogItemRepository.DeleteItem(globalId);
        }
        public List<CatalogItem> GetCatalogItems(int skip, int count)
        {
            return _mapper.Map<List<CatalogItem>>(_catalogItemRepository.GetCatalogItemViews(skip, count));
        }

        public int GetCatalogItemsCount()
        {
            return _catalogItemRepository.GetCatalogItemsCount();
        }
        public CatalogItemType GetTypeById(int id)
        {
            return _catalogItemRepository.Get(id).Type;         
        }
    }
}