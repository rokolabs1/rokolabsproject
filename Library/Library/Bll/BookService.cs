﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.DAL;
using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Library.BLL
{
    public class BookService : BaseService<Book>, IBookService
	{
		private readonly ILogger<BookService> _logger;
		private readonly IMapper _mapper;
		private readonly IBookRepository _bookRepository;
        private readonly IAuthorService _authorService;
		private readonly ICityService _cityService;
		private readonly IPublishingOfficeService _publishingOfficeService;
		private readonly ICatalogItemRepository _catalogItemRepository;

        public BookService(
            ILogger<BookService> logger,
            IMapper mapper,
            IBookRepository bookRepository,
            IAuthorService authorService,
            ICityService cityService,
            IPublishingOfficeService publishingOfficeService,
            ICatalogItemRepository catalogItemRepository,
            IValidator<Book> validator) : base(validator)
        {
            _logger = logger;
            _bookRepository = bookRepository;
            _authorService = authorService;
            _cityService = cityService;
            _publishingOfficeService = publishingOfficeService;
            _mapper = mapper;
            _catalogItemRepository = catalogItemRepository;
        }

        public List<Book> GetBooks(IEnumerable<int> ids = null)
        {
            var bookDtos = _bookRepository.GetBooks(ids);

            var authors = _authorService.GetAuthorsByIds(bookDtos.SelectMany(p => p.AuthorIds))
                .ToDictionary(a => a.Id);
            var publishingOffices = _publishingOfficeService.Get(bookDtos.Select(a => a.PublishingOfficeId))
             .ToDictionary(a => a.Id);
            var cities = _cityService.Get(bookDtos.Select(a => a.CityId))
                .ToDictionary(a => a.Id);

            var books = new List<Book>(bookDtos.Count);

            foreach (var dto in bookDtos)
            {
                var book = _mapper.Map<Book>(dto);
				book.Authors = dto.AuthorIds.Select(id => authors[id]).ToList();
                book.City = cities[dto.CityId];
                book.PublishingOffice = publishingOffices[dto.PublishingOfficeId];
                books.Add(book);
            }
            return books;
        }
        public void DeleteBook(int id)
		{
			_bookRepository.DeleteBook(id);
		}
		public int InsertBook(Book book)
		{
            Validate(book);

            _authorService.InsertAuthors(book.Authors);
            _cityService.InsertCity(book.City);
			_publishingOfficeService.InsertPublishingOffice(book.PublishingOffice);

            book.Id = _catalogItemRepository.Insert(new CatalogItemDTO { Type = CatalogItemType.Book });
			 
			_bookRepository.InsertBook(_mapper.Map<BookDTO>(book));

            return book.Id;
		}

        public void UpdateBook(Book book)
        {
            Validate(book);

            _authorService.InsertAuthors(book.Authors);
            _cityService.InsertCity(book.City);
            _publishingOfficeService.InsertPublishingOffice(book.PublishingOffice);

            _bookRepository.UpdateBook(book.Id, _mapper.Map<BookDTO>(book));
        }
    }
}
