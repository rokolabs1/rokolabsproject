﻿using AutoMapper;
using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Logging;
using FluentValidation;
using Library.BLL.Interfaces;
using System.Collections.Generic;

namespace Library.BLL
{
    public class UserService: BaseService<User>, IUserService
    {
        private readonly ILogger<UserService> _logger;
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(
            ILogger<UserService> logger, 
            IUserRepository userRepository, 
            IMapper mapper,
            IValidator<User> validator) : base(validator)
        {
            _logger = logger;
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public void ChangeUserRole(User user)
        {
            _userRepository.ChangeUserRole(_mapper.Map<UserDTO>(user));
        }

        public List<User> GetUsers()
        {
            return _mapper.Map<List<User>>(_userRepository.GetUsers());
        }
        public User GetUserById(int id)
        {
            return _mapper.Map<User>(_userRepository.GetUserById(id));
        }

        public User Login (User user)
        {
            Validate(user);
            user.Login = user.Login.ToLower();
            user.Password = HashHelper.GetHashString(user.Password);
            return _mapper.Map<User>(_userRepository.Login(_mapper.Map<UserDTO>(user)));
        }
        public User Register(User user)
        {
            Validate(user);
            user.Role = "guest";
            user.Password = HashHelper.GetHashString(user.Password);
            int id = _userRepository.Register(_mapper.Map<UserDTO>(user));
            user.Id = id;
            user.Password = null;
            return user;
        }
    }
}
