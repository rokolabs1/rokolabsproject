﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.DAL;
using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Library.BLL
{
    public class NewspaperService: BaseService<Newspaper>, INewspaperService
    {
		private readonly ILogger<NewspaperService> _logger;
		private readonly INewspaperRepository _newspaperRepository;
		private readonly ICityService _cityService;
		private readonly IPublishingOfficeService _publishingOfficeService;
		private readonly ICatalogItemRepository _catalogItemRepository;
		private readonly IMapper _mapper;
		public NewspaperService(
			ILogger<NewspaperService> logger,
			IMapper mapper,
			INewspaperRepository newspaperRepository,
			ICityService cityService,
			IPublishingOfficeService publishingOfficeService,
			ICatalogItemRepository catalogItemRepository,
			IValidator<Newspaper> validator) : base(validator)
		{
			_logger = logger;
			_newspaperRepository = newspaperRepository;
			_cityService = cityService;
			_publishingOfficeService = publishingOfficeService;
			_catalogItemRepository = catalogItemRepository;
			_mapper = mapper;
		}

		public List<Newspaper> GetNewspapers(IEnumerable<int> ids = null)
		{
			var newspaperDtos = _newspaperRepository.GetNewspapers(ids);

			var publishingOffices = _publishingOfficeService.Get(newspaperDtos.Select(a => a.PublishingOfficeId))
			 .ToDictionary(a => a.Id);
			var cities = _cityService.Get(newspaperDtos.Select(a => a.CityId))
				.ToDictionary(a => a.Id);

			var newspapers = new List<Newspaper>(newspaperDtos.Count);

			foreach (var dto in newspaperDtos)
			{
				var newspaper = _mapper.Map<Newspaper>(dto);
				newspaper.City = cities[dto.CityId];
				newspaper.PublishingOffice = publishingOffices[dto.PublishingOfficeId];
				newspapers.Add(newspaper);
			}
			return newspapers;
		}
		public void DeleteNewspaper(int id)
		{
			_newspaperRepository.DeleteNewspaper(id);
		}
		public int InsertNewspaper(Newspaper newspaper)
		{
			Validate(newspaper);

			_cityService.InsertCity(newspaper.City);
			_publishingOfficeService.InsertPublishingOffice(newspaper.PublishingOffice);

			newspaper.Id = _catalogItemRepository.Insert(new CatalogItemDTO { Type = CatalogItemType.Newspaper });

			_newspaperRepository.InsertNewspaper(_mapper.Map<NewspaperDTO>(newspaper));

			return newspaper.Id;
		}
		public void UpdateNewspaper(Newspaper newspaper)
		{
			Validate(newspaper);

			_cityService.InsertCity(newspaper.City);
			_publishingOfficeService.InsertPublishingOffice(newspaper.PublishingOffice);

			_newspaperRepository.UpdateNewspaper(newspaper.Id, _mapper.Map<NewspaperDTO>(newspaper));
		}
		public List<Newspaper> GetNewspaperIssues(int id)
        {
			return _mapper.Map<List<Newspaper>>(_newspaperRepository.GetNewspaperIssues(id));
        }
	}
}
