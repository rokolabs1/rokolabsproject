﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Library.BLL
{
    public class PublishingOfficeService : BaseService<PublishingOffice>, IPublishingOfficeService
    {
        private readonly ILogger<PublishingOfficeService> _logger;
        private readonly IMapper _mapper;
        private readonly IPublishingOfficeRepository _publishingOfficeRepository;

        public PublishingOfficeService(
            ILogger<PublishingOfficeService> logger,
            IPublishingOfficeRepository publishingOfficeRepository,
            IValidator<PublishingOffice> validator,
            IMapper mapper) : base(validator)
        {
            _logger = logger;
            _publishingOfficeRepository = publishingOfficeRepository;
            _mapper = mapper;
        }

        public int InsertPublishingOffice(PublishingOffice publishingOffice)
        {
            Validate(publishingOffice);

            if (publishingOffice.Id == 0)
            {
                publishingOffice.Id = _publishingOfficeRepository.InsertIfNotExists(_mapper.Map<PublishingOfficeDTO>(publishingOffice));
            }

            return publishingOffice.Id;
        }
        public List<PublishingOffice> Get(IEnumerable<int> ids)
        {
            return _mapper.Map<List<PublishingOffice>>(_publishingOfficeRepository.Get(ids));
        }
    }
}
