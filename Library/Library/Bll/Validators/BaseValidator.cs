﻿using FluentValidation;

namespace Library.BLL.Validators
{
    public class BaseValidator<T>: AbstractValidator<T>
    {
        public BaseValidator()
        {
            CascadeMode = CascadeMode.StopOnFirstFailure;
        }
    }
}
