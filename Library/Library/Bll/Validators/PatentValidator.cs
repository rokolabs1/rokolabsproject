﻿using FluentValidation;
using Library.Entities;
using System;

namespace Library.BLL.Validators
{
    public class PatentValidator : BaseValidator<Patent>
    {
        public PatentValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("The title cannot be empty.")
                .MaximumLength(300).WithMessage("The title is no more than 300 characters long."); 

            RuleFor(x => x.Authors)
                .NotEmpty().WithMessage("The authors cannot be empty.");

            RuleFor(x => x.Country)
                .NotEmpty().WithMessage("The country cannot be empty.");

            RuleFor(x => x.NumberRegistration)
                .GreaterThan(0);

            RuleFor(x => x.PublicationDate)
                .LessThanOrEqualTo(DateTime.Now)
                .Custom((date, context) =>
                {
                    if (date < context.InstanceToValidate.ApplicationDate)
                    {
                        context.AddFailure("PublicationDate is less then ApplicationDate");
                    }
                });

            RuleFor(x => x.PageCount)
                .GreaterThan((short)0);

            RuleFor(x => x.Description)
                .MaximumLength(2000);
        }
    }
}
