﻿using FluentValidation;
using Library.Entities;
using System;

namespace Library.BLL.Validators
{
    public class UserValidator : BaseValidator<User>
    {
        public UserValidator()
        {
            RuleFor(x => x.Login)
                .NotEmpty().WithMessage("The login must not empty.")
                .Must(l => !l.StartsWith('_')).WithMessage("The login cannot start with a character '_'.")
                .Must(l => !Char.IsNumber(l[0])).WithMessage("The login cannot start with a number.")
                .Must(l => !l.EndsWith('_')).WithMessage("The login cannot end with a character '_'.")
                .Must(l => !l.Contains("__")).WithMessage("The login cannot contain two characters '_' in a row.")
                .Must(l => ValidatorHelper.IsLettersOfLatinAlphabetOrAllowedChars(l)).WithMessage("The login can only consist of letters of the Latin alphabet, characters '_' and numbers. ");

            RuleFor(x => x.Password)
                .NotEmpty().WithMessage("The password must not empty.")
                .MinimumLength(3).WithMessage("The password is no more than 3 characters long.")
                .Must((u, p) => !p.Contains(u.Login)).WithMessage("The password should not contain login.").When((u, p) => u.Login != null)
                .Must(p => ValidatorHelper.IsAnyCharacterOtherThanControlCharacters(p)).WithMessage("The password cannot contain any of the control characters.");
        }
    }
}
