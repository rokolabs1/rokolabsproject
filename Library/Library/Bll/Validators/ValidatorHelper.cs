﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.BLL.Validators
{
    public static class ValidatorHelper
    {
        private static List<SupportedLanguage> _supportedLanguages = new List<SupportedLanguage>
        {
            new SupportedLanguage("English", 'a', 'z'),
            new SupportedLanguage("Russian", 'а', 'я'),
        };

        public static bool IsSingleLanguage(string value)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return true;
            }

            string language = null;
            foreach (char ch in value.Where(x => char.IsLetter(x)))
            {
                string charLanguage = GetLanguage(ch);
                if (charLanguage == null)
                {
                    continue;
                }

                if (language == null)
                {
                    language = charLanguage;
                }
                else if (language != charLanguage)
                {
                    return false;
                }
            }

            return true;
        }

        public static string GetLanguage(char value)
        {
            char lower = char.ToLower(value);
            foreach (SupportedLanguage lng in _supportedLanguages)
            {
                if (lng.From <= lower && lower <= lng.To)
                {
                    return lng.Name;
                }
            }
            return null;
        }
    
        public static bool IsFirstAndLastSymbolsAreLetters(string value)
        {
            return !string.IsNullOrWhiteSpace(value) 
                && char.IsLetter(value[0])
                && char.IsLetter(value[value.Length - 1]);
        }

        public static bool IsLettersOrAllowedChars(string value, params char[] allowedChars)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return false;
            }

            if (allowedChars == null)
            {
                allowedChars = new char[0];
            }
            var hashSet = new HashSet<char>(allowedChars);
            return value.All(x => char.IsLetter(x) || hashSet.Contains(x));
        }

        public static bool IsUpperAfterSeparators(string value, params string[] separators)
        {
            if (string.IsNullOrWhiteSpace(value))
            {
                return false;
            }

            if (separators == null || separators.Length == 0)
            {
                return char.IsUpper(value[0]);
            }

            var parts = value.Split(separators, StringSplitOptions.RemoveEmptyEntries);
            return parts.All(p => char.IsUpper(p[0]));
        }
        public static bool IsLettersOfLatinAlphabetOrAllowedChars(string value, params char[] allowedChars)
        {
            string lower = value.ToLower();
            SupportedLanguage language = new SupportedLanguage("Latin", 'a', 'z');
            for (int i = 0; i < lower.Length; i++)
            {
                if (char.IsLetter(lower[i]))
                {
                    if(language.From > lower[i] || lower[i] > language.To)
                    {
                        return false;
                    }                  
                }
                else 
                {
                    if (!char.IsDigit(lower[i]) && lower[i] != '_')
                    {
                        return false;
                    }
                }
            }
            return true;
        }
        public static bool IsAnyCharacterOtherThanControlCharacters (string value)
        {
            for (int i = 0; i < value.Length; i++)
            {
                if(value[i] < 32)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
