﻿using FluentValidation;
using Library.Entities;
using System.Linq;

namespace Library.BLL.Validators
{
    public class CountryValidator : BaseValidator<Country>
    {
        public CountryValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(200)
                .Must(ValidatorHelper.IsSingleLanguage)
                .Must(x => ValidatorHelper.IsUpperAfterSeparators(x) || x.All(ch => char.IsUpper(ch)));
        }
    }
}
