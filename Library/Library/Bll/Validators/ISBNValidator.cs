﻿using FluentValidation;
using System.Linq;

namespace Library.BLL.Validators
{
    public class ISBNValidator : BaseValidator<string>
    {
        public const string Prefix = "ISBN ";
        public const byte DigitsCount = 10;
        public const byte DigitGroupsCount = 4;

        public ISBNValidator()
        {
            RuleFor(x => x)
                .Cascade(CascadeMode.StopOnFirstFailure)
                .NotEmpty().WithMessage("The ISBN cannot be empty.")
                .Length(Prefix.Length + DigitsCount + (DigitGroupsCount - 1)).WithMessage("The ISBN length 19 characters.")
                .Must(HasCorrectDigitsCount).WithMessage("The code must contain 10 digits.")
                .Must(StartsWithISBN).WithMessage("The ISBN must start with characters \"ISBN\".")
                .Must(x => HasCorrectDigitGroups(x.Substring(Prefix.Length))).WithMessage("The code should consist of 4 groups, separated hyphens.");
        }

        public static bool StartsWithISBN(string value)
        {
            return value.StartsWith(Prefix);
        }

        public static bool HasCorrectDigitsCount(string value)
        {
            var arabianDigitsCount = value.Count(x => char.IsDigit(x));
            var xCount = value.Count(x => x == 'X');

            return (arabianDigitsCount + xCount) == DigitsCount;
        }

        public static bool HasCorrectDigitGroups(string value)
        {
            var groups = value.Split('-');

            if (groups.Length != 4)
            {
                return false;
            }

            var group = groups[0];
            if (!int.TryParse(group, out int groupValue)
                || !(
                 (0 <= groupValue && groupValue <= 7)
                 || (80 <= groupValue && groupValue <= 94)
                 || (950 <= groupValue && groupValue <= 993)
                 || (9940 <= groupValue && groupValue <= 9989)
                 || (99900 <= groupValue && groupValue <= 99999)
                ))
            {
                return false;
            }

            group = groups[1];
            if (group.Length > 7
                || group.Length < 1
                || group.Any(x => !char.IsDigit(x)))
            {
                return false;
            }

            group = groups[2];
            if (group.Length > 7
                || group.Length < 1
                || group.Any(x => !char.IsDigit(x)))
            {
                return false;
            }

            group = groups[3];
            if (group.Length != 1
                || (!char.IsDigit(group[0]) && group != "X"))
            {
                return false;
            }

            return true;
        }
    }
}
