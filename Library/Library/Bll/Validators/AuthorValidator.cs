﻿using FluentValidation;
using Library.Entities;


namespace Library.BLL.Validators
{
    public class AuthorValidator : BaseValidator<Author>
    {
        private static string[] _lastNameSeparators = new string[]
        {
            "'",
            "-",
            "фон ",
            "ди ",
            "де ",
            "ван ",
            "di ",
            "de ",
            "fon ",
            "van ",
        };

        public AuthorValidator()
        {
            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage("The first name must not empty.")
                .MaximumLength(50).WithMessage("The first name is no more than 50 characters long.")
                .Must(ValidatorHelper.IsSingleLanguage).WithMessage("All letters in the first name must be in the same language.")
                .Must(fn => ValidatorHelper.IsLettersOrAllowedChars(fn, '-'))
                .Must(ValidatorHelper.IsFirstAndLastSymbolsAreLetters)
                .Must(fn => ValidatorHelper.IsUpperAfterSeparators(fn, "-"));

            RuleFor(x => x.LastName)
                .NotEmpty().WithMessage("The last name must not empty.")
                .MaximumLength(200).WithMessage("The last name is no more than 200 characters long.")
                .Must(ValidatorHelper.IsSingleLanguage).WithMessage("All letters in the last name must be in the same language.")
                .Must(ln => ValidatorHelper.IsLettersOrAllowedChars(ln, '-', ' ', '\''))
                .Must(ValidatorHelper.IsFirstAndLastSymbolsAreLetters)
                .Must(ln => ValidatorHelper.IsUpperAfterSeparators(ln, _lastNameSeparators));
        }
    }
}
