﻿using FluentValidation;
using Library.Entities;

namespace Library.BLL.Validators
{
    public class CityValidator : BaseValidator<City>
    {
        public CityValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(200)
                .Must(ValidatorHelper.IsSingleLanguage)
                .Must(ValidatorHelper.IsFirstAndLastSymbolsAreLetters)
                .Must(x => ValidatorHelper.IsLettersOrAllowedChars(x, '-', ' '))
                .Must(x => ValidatorHelper.IsUpperAfterSeparators(x, "-"));
        }
    }
}
