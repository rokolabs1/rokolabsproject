﻿using FluentValidation;
using Library.Entities;
using System;

namespace Library.BLL.Validators
{
    public class BookValidator : BaseValidator<Book>
    {
        public BookValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("The title cannot be empty.")
                .MaximumLength(300).WithMessage("The title is no more than 300 characters long."); 

            RuleFor(x => x.Authors)
                .NotEmpty();

            RuleFor(x => x.City)
                .NotEmpty();

            RuleFor(x => x.PublishingOffice)
                .NotEmpty();

            RuleFor(x => x.PublishingYear)
                .LessThanOrEqualTo(DateTime.Today.Year);

            RuleFor(x => x.PageCount)
                .GreaterThan((short)0);

            RuleFor(x => x.Description)
                .MaximumLength(2000);

            RuleFor(x => x.ISBN)
                .SetValidator(new ISBNValidator())
                .When(x => !string.IsNullOrWhiteSpace(x.ISBN));
        }
    }
}
