﻿using FluentValidation;
using Library.Entities;

namespace Library.BLL.Validators
{
    public class PublishingOfficeValidator : BaseValidator<PublishingOffice>
    {
        public PublishingOfficeValidator()
        {
            RuleFor(x => x.Name)
                .NotEmpty()
                .MaximumLength(300);
        }
    }
}
