﻿using FluentValidation;
using System.Text.RegularExpressions;

namespace Library.BLL.Validators
{
    public class ISSNValidator : BaseValidator<string>
    {
        public const string RegexPattern = @"^ISSN(\d\d-){3}\d\d$";

        public ISSNValidator()
        {
            RuleFor(x => x)
                .NotEmpty().WithMessage("ISBN cannot be empty.")
                .Must(x => Regex.IsMatch(x, RegexPattern)).WithMessage("The ISSN consists of the characters \"ISSN\" and two four-digit numeric groups separated by a hyphen.");
        }
    }
}
