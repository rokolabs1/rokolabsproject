﻿using FluentValidation;
using Library.Entities;
using System;

namespace Library.BLL.Validators
{
    public class NewspaperValidator : BaseValidator<Newspaper>
    {
        public NewspaperValidator()
        {
            RuleFor(x => x.Title)
                .NotEmpty().WithMessage("The title cannot be empty.")
                .MaximumLength(300).WithMessage("The title is no more than 300 characters long.");

            RuleFor(x => x.City)
                .NotEmpty().WithMessage("The city cannot be empty.");

            RuleFor(x => x.PublishingOffice)
                .NotEmpty().WithMessage("The publishing office cannot be empty.");

            RuleFor(x => x.PublicationYear)
                .LessThanOrEqualTo(DateTime.Today.Year).WithMessage("The year of publication should not be more than the current year.");

            RuleFor(x => x.PageCount)
                .GreaterThan((short)0).WithMessage("The number of pages cannot be less than 0.");

            RuleFor(x => x.Description)
                .MaximumLength(2000).WithMessage("The description is no more than 2000 characters long.");

            RuleFor(x => x.Number)
                .GreaterThan(0).WithMessage("The number cannot be less than 0.");

            RuleFor(x => x.PublicationDate)
                .Custom((date, context) =>
                {
                    if (date.Year != context.InstanceToValidate.PublicationYear)
                    {
                        context.AddFailure("PublicationDate.Year is not equal to PublicationYear");
                    }
                });

            RuleFor(x => x.ISSN)
                .SetValidator(new ISSNValidator())
                .When(x => !string.IsNullOrWhiteSpace(x.ISSN));
        }
    }
}
