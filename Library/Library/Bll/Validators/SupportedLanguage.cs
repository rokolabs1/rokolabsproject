﻿namespace Library.BLL.Validators
{
    internal class SupportedLanguage
    {
        public string Name { get; set; }
        public char From { get; set; }
        public char To { get; set; }

        public SupportedLanguage(string name, char from, char to)
        {
            Name = name;
            From = from;
            To = to;
        }
    }
}
