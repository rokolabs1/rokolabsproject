﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Library.BLL
{
    public class CountryService : BaseService<Country>,ICountryService
    {
        private readonly ILogger<CountryService> _logger;
        private readonly IMapper _mapper;
        private readonly ICountryRepository _countryRepository;

        public CountryService(
            ILogger<CountryService> logger,
            ICountryRepository countryRepository,
            IValidator<Country> validator,
            IMapper mapper) : base(validator)
        {
            _logger = logger;
            _countryRepository = countryRepository;
            _mapper = mapper;
        }

        public int InsertCountry(Country country)
        {
            Validate(country);

            if (country.Id == 0)
            {
                country.Id = _countryRepository.InsertIfNotExists(_mapper.Map<CountryDTO>(country));
            }

            return country.Id;
        }
        public List<Country> Get(IEnumerable<int> ids)
        {
            return _mapper.Map<List<Country>>(_countryRepository.Get(ids));
        }
    }
}
