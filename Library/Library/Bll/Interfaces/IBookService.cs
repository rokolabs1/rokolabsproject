﻿using Library.Entities;
using System.Collections.Generic;

namespace Library.BLL.Interfaces
{
    public interface IBookService
	{
		List<Book> GetBooks(IEnumerable<int> ids = null);
		void DeleteBook(int id);
		int InsertBook(Book book);
		void UpdateBook(Book book);
	}
}
