﻿using Library.Entities;
using System.Collections.Generic;

namespace Library.BLL.Interfaces
{
    public interface IUserService
    {
        User Login(User user);
        User Register(User user);
        List<User> GetUsers();
        void ChangeUserRole(User user);
        User GetUserById(int id);
    }
}
