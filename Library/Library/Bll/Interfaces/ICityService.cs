﻿using Library.Entities;
using System.Collections.Generic;

namespace Library.BLL.Interfaces
{
    public interface ICityService
    {
        int InsertCity(City city);
        List<City> Get(IEnumerable<int> ids);
    }
}
