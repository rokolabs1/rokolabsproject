﻿using Library.DAL;
using Library.Entities;
using System.Collections.Generic;

namespace Library.BLL.Interfaces
{
    public interface ICatalogService
    {
        void DeleteItem(int globalId);
        List<CatalogItem> GetCatalogItems(int skip, int count);
        CatalogItemType GetTypeById(int id);
        int GetCatalogItemsCount();
    }
}
