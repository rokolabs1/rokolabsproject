﻿using Library.Entities;
using System.Collections.Generic;

namespace Library.BLL.Interfaces
{
    public interface IPublishingOfficeService
    {
        int InsertPublishingOffice(PublishingOffice publishingOffice);
        List<PublishingOffice> Get(IEnumerable<int> ids);
    }
}
