﻿using Library.Entities;
using System.Collections.Generic;

namespace Library.BLL.Interfaces
{
    public interface INewspaperService
    {
        List<Newspaper> GetNewspapers(IEnumerable<int> ids = null);
        void DeleteNewspaper(int id);
        int InsertNewspaper(Newspaper newspaper);
        void UpdateNewspaper(Newspaper newspaper);
        List<Newspaper> GetNewspaperIssues(int id);
    }
}
