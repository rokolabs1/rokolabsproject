﻿using Library.Entities;
using System.Collections.Generic;

namespace Library.BLL.Interfaces
{
	public interface IAuthorService
	{
		List<Author> GetAuthors();
		List<Author> GetAuthorsByIds(IEnumerable<int> ids);
		int InsertAuthor(Author author);
		void InsertAuthors(IEnumerable<Author> authors);
		void UpdateAuthor(int id, Author author);
		void DeleteAuthor(int id);
	}
}
