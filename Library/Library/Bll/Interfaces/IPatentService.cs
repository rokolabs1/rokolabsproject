﻿using Library.Entities;
using System.Collections.Generic;

namespace Library.BLL.Interfaces
{
    public interface IPatentService
    {
        List<Patent> GetPatents(IEnumerable<int> ids = null);
        void DeletePatent(int id);
        int InsertPatent(Patent patent);
        void UpdatePatent(Patent patent);
    }
}
