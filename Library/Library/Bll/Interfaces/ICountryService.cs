﻿using Library.Entities;
using System.Collections.Generic;

namespace Library.BLL.Interfaces
{
    public interface ICountryService
    {
        int InsertCountry(Country country);
        List<Country> Get(IEnumerable<int> ids);
    }
}
