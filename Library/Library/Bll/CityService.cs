﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Library.BLL
{
    public class CityService : BaseService<City>, ICityService
    {
        private readonly ILogger<CityService> _logger;
		private readonly IMapper _mapper;
        private readonly ICityRepository _cityRepository;

        public CityService(
            ILogger<CityService> logger,
            ICityRepository cityRepository,
            IValidator<City> validator,
            IMapper mapper) : base(validator)
        {
            _logger = logger;
            _cityRepository = cityRepository;
            _mapper = mapper;
        }

        public int InsertCity(City city)
        {
            Validate(city);

            if (city.Id == 0)
            {
                city.Id = _cityRepository.InsertIfNotExists(_mapper.Map<CityDTO>(city));
            }

            return city.Id;
        }
        public List<City> Get(IEnumerable<int> ids)
        {
            return _mapper.Map<List<City>>(_cityRepository.Get(ids));
        }
    }
}
