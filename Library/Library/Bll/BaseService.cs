﻿using FluentValidation;

namespace Library.BLL
{
    public abstract class BaseService<T>
    {
        private readonly IValidator<T> _validator;
        public BaseService(IValidator<T> validator)
        {
            _validator = validator;
        }

        protected void Validate(T entity)
        {
            var validationResult = _validator.Validate(entity);
            if (!validationResult.IsValid)
            {
                throw new ValidationException(validationResult.Errors);
            }
        }
    }
}
