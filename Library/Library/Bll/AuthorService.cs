﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Library.BLL
{
	public class AuthorService : BaseService<Author>, IAuthorService
	{
		private readonly ILogger<AuthorService> _logger;
		private readonly IAuthorRepository _authorRepository;
		private readonly IMapper _mapper;
		public AuthorService(
			ILogger<AuthorService> logger,
			IMapper mapper,
			IAuthorRepository authorRepository,
			IValidator<Author> validator) : base(validator)
		{
			_logger = logger;
			_authorRepository = authorRepository;
			_mapper = mapper;
		}

		public void DeleteAuthor(int id)
		{
			_authorRepository.DeleteAuthor(id);
		}

		public List<Author> GetAuthors()
		{
			return _mapper.Map<List<Author>>(_authorRepository.GetAuthors());
		}

		public List<Author> GetAuthorsByIds(IEnumerable<int> ids)
        {
			return _mapper.Map<List<Author>>(_authorRepository.GetAuthorsByIds(ids));
		}

		public int InsertAuthor(Author author)
		{
			Validate(author);
			author.Id = _authorRepository.InsertAuthor(_mapper.Map<AuthorDTO>(author));
			return author.Id;
		}

		public void UpdateAuthor(int id, Author author)
		{
			Validate(author);

			_authorRepository.UpdateAuthor(id, _mapper.Map<AuthorDTO>(author));
		}

		public void InsertAuthors(IEnumerable<Author> authors)
		{
			foreach(var author in authors)
            {
				if (author.Id == 0)
                {
					Validate(author);
				}
            }

			foreach (var author in authors)
			{
				if (author.Id == 0)
                {
					author.Id = _authorRepository.InsertAuthor(_mapper.Map<AuthorDTO>(author));
				}
			}
		}
	}
}
