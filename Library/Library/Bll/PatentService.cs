﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.DAL;
using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Library.BLL
{
    public class PatentService: BaseService<Patent>, IPatentService
    {
		private readonly ILogger<PatentService> _logger;
		private readonly IPatentRepository _patentRepository;
		private readonly IAuthorService _authorService;
		private readonly ICountryService _countryService;
		private readonly ICatalogItemRepository _catalogItemRepository;
		private readonly IMapper _mapper;
		public PatentService(
			ILogger<PatentService> logger,
			IMapper mapper,
			IPatentRepository patentRepository,
			IAuthorService authorService,
			ICountryService countryService,
			ICatalogItemRepository catalogItemRepository,
			IValidator<Patent> validator) : base(validator)
		{
			_logger = logger;
			_patentRepository = patentRepository;
			_authorService = authorService;
			_countryService = countryService;
			_catalogItemRepository = catalogItemRepository;
			_mapper = mapper;
		}

		public List<Patent> GetPatents(IEnumerable<int> ids = null)
		{
			var patentDtos = _patentRepository.GetPatents(ids);

			var authors = _authorService.GetAuthorsByIds(patentDtos.SelectMany(p => p.AuthorIds))
				.ToDictionary(a => a.Id);
			var countries = _countryService.Get(patentDtos.Select(a => a.CountryId))
				.ToDictionary(a => a.Id);

			var patents = new List<Patent>(patentDtos.Count);

			foreach (var dto in patentDtos)
			{
				var patent = _mapper.Map<Patent>(dto);
				patent.Authors = dto.AuthorIds.Select(id => authors[id]).ToList();
				patent.Country = countries[dto.CountryId];
				patents.Add(patent);
			}
			return patents;
		}
		public void DeletePatent(int id)
		{
			_patentRepository.DeletePatent(id);
		}
		public int InsertPatent(Patent patent)
		{
			Validate(patent);

			_authorService.InsertAuthors(patent.Authors);
			_countryService.InsertCountry(patent.Country);

			patent.Id = _catalogItemRepository.Insert(new CatalogItemDTO { Type = CatalogItemType.Patent });

			_patentRepository.InsertPatent(_mapper.Map<PatentDTO>(patent));

			return patent.Id;
		}

		public void UpdatePatent(Patent patent)
		{
			Validate(patent);

			_authorService.InsertAuthors(patent.Authors);
			_countryService.InsertCountry(patent.Country);

			_patentRepository.UpdatePatent(patent.Id, _mapper.Map<PatentDTO>(patent));
		}
	}
}
