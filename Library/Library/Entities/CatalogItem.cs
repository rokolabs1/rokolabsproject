﻿namespace Library.Entities
{
    public class CatalogItem
    {
        public int CatalogItemId { get; set; }
        public string Name { get; set; }
        public string PublicId { get; set; }
        public int PageCount { get; set; }
    }
}
