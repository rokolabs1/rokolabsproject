﻿namespace Library.Entities
{
    public class PublishingOffice
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
