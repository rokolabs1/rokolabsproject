﻿using System;
using System.Collections.Generic;

namespace Library.Entities
{
    public class Patent
	{
		public int Id { get; set; }
		public string Title { get; set; }
        public short PageCount { get; set; }
        public Country Country { get; set; }
        public int NumberRegistration { get; set; }
        public DateTime PublicationDate { get; set; }
		public DateTime ApplicationDate { get; set; }
		public List<Author> Authors { get; set; }
		public string Description { get; set; }
	}
}
