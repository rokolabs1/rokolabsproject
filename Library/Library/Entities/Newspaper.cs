﻿using System;

namespace Library.Entities
{
    public class Newspaper
	{
		public int Id { get; set; }
		public string Title { get; set; }
        public int? Number { get; set; }
        public short PageCount { get; set; }
        public City City { get; set; }
        public string ISSN { get; set; }
        public int PublicationYear { get; set; }
		public DateTime PublicationDate { get; set; }
		public PublishingOffice PublishingOffice { get; set; }
		public string Description { get; set; }
	}
}
