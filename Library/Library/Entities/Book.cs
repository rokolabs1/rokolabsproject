﻿using System.Collections.Generic;

namespace Library.Entities
{
    public class Book
	{
		public int Id { get; set; }
		public string Title { get; set; }
        public short PageCount { get; set; }
        public City City { get; set; }
        public int PublishingYear { get; set; }
        public string ISBN { get; set; }
        public PublishingOffice PublishingOffice { get; set; }
		public List<Author> Authors { get; set; }
		public string Description { get; set; }
	}
}
