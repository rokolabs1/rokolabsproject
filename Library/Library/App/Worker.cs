﻿using Library.BLL.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Library
{
	public class Worker : BackgroundService
	{
		private readonly ILogger<Worker> _logger;
		private readonly IAuthorService _authorService;
		private readonly IBookService _bookService;

        public Worker(ILogger<Worker> logger, IAuthorService authorService, IBookService bookService)
        {
            _logger = logger;
            _authorService = authorService;
            _bookService = bookService;
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			List<Author> authors = _authorService.GetAuthors();

			ShowAuthors(authors);

			int id = _authorService.InsertAuthor(new Author() { FirstName = "New", LastName = "Author" });

			authors = _authorService.GetAuthors();
			ShowAuthors(authors);

			_authorService.UpdateAuthor(id, new Author() { Id = id, FirstName = "Updated", LastName = "Author" });

			authors = _authorService.GetAuthors();
			ShowAuthors(authors);

			//_authorService.DeleteAuthor(id);
			authors = _authorService.GetAuthors();
			ShowAuthors(authors);

			var book = new Book()
			{
				City = new City { Name = "Cityone", },
				PublishingOffice = new PublishingOffice { Name = "PublishingOffice1" },
				Title = "Title1",
				PageCount = 10,
				Authors = new List<Author>(authors.Take(3)),
			};
			_bookService.InsertBook(book);

            List<Book> books = _bookService.GetBooks();

			book.Title = "Title2";
			_bookService.UpdateBook(book);
            ShowBooks(books);

            return Task.CompletedTask;
		}

		private static void ShowAuthors(List<Author> authors)
		{
			foreach (var author in authors)
			{
				Console.WriteLine($"{author.Id} {author.FirstName} {author.LastName}");
			}
			Console.WriteLine();
		}

		private static void ShowBooks(List<Book> books)
		{
			foreach (var book in books)
			{
				Console.WriteLine($"{book.Id} {book.Title}");
				ShowAuthors(book.Authors);
			}
			Console.WriteLine();
		}
	}
}