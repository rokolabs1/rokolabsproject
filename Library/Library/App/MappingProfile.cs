﻿using AutoMapper;
using Library.DAL.DTO;
using Library.Entities;
using System.Linq;

namespace Library.App
{
    public class MappingProfile : Profile
	{
		public MappingProfile()
		{
            CreateMap<City, CityDTO>();
            CreateMap<CityDTO, City>();
            
            CreateMap<Country, CountryDTO>();
            CreateMap<CountryDTO, Country>();

            CreateMap<PublishingOffice, PublishingOfficeDTO>();
            CreateMap<PublishingOfficeDTO, PublishingOffice>();

            CreateMap<Author, AuthorDTO>();
            CreateMap<AuthorDTO, Author>();

            CreateMap<Book, BookDTO>()
                .ForMember(obj => obj.CityId, opt => opt.MapFrom(obj => obj.City.Id))
                .ForMember(obj => obj.PublishingOfficeId, opt => opt.MapFrom(obj => obj.PublishingOffice.Id))
                .ForMember(obj => obj.AuthorIds, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.Id).ToList()));
            CreateMap<BookDTO, Book>();

            CreateMap<Patent, PatentDTO>()
                .ForMember(obj => obj.CountryId, opt => opt.MapFrom(obj => obj.Country.Id))
                .ForMember(obj => obj.AuthorIds, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.Id).ToList()));
            CreateMap<PatentDTO, Patent>()
                .ForPath(obj => obj.Country.Id, opt => opt.MapFrom(obj => obj.CountryId))
                .ForMember(obj => obj.Authors, opt => opt.MapFrom(obj => obj.AuthorIds.Select(a => new Author { Id = a })));

            CreateMap<Newspaper, NewspaperDTO>()
                .ForMember(obj => obj.CityId, opt => opt.MapFrom(obj => obj.City.Id))
                .ForMember(obj => obj.PublishingOfficeId, opt => opt.MapFrom(obj => obj.PublishingOffice.Id));
            CreateMap<NewspaperDTO, Newspaper>()
                .ForPath(obj => obj.City.Id, opt => opt.MapFrom(obj => obj.CityId))
                .ForPath(obj => obj.PublishingOffice.Id, opt => opt.MapFrom(obj => obj.PublishingOfficeId));

            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();
        }
	}
}
