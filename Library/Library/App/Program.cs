﻿using AutoMapper;
using Library.DAL;
using Library.DAL.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Library.BLL.Interfaces;
using Library.BLL;
using FluentValidation;
using Library.BLL.Validators;

namespace Library.App
{
    class Program
	{
		private static IHostBuilder CreateHostBuilder(string[] args)
		{
			return Host.CreateDefaultBuilder(args)
				.ConfigureServices((hostingContext, services) =>
				{
					IConfiguration configuration = hostingContext.Configuration;
					var connectionString = configuration.GetConnectionString("DefaultConnection");

					var mappingConfig = new MapperConfiguration(mc =>
					{
						mc.AddProfile(new MappingProfile());
					});

					IMapper mapper = mappingConfig.CreateMapper();
					services.AddSingleton(mapper);

					services.AddSingleton(new DBOptions() { ConnectionString = connectionString });

					services.AddTransient<ICityService, CityService>();
					services.AddTransient<ICountryService, CountryService>();
					services.AddTransient<IPublishingOfficeService, PublishingOfficeService>();
					services.AddTransient<IAuthorService, AuthorService>();
					services.AddTransient<ICatalogService, CatalogService>();
					services.AddTransient<IBookService, BookService>();
					services.AddTransient<INewspaperService, NewspaperService>();
					services.AddTransient<IPatentService, PatentService>();

					services.AddTransient<ICityRepository, CityRepository>();
					services.AddTransient<ICountryRepository, CountryRepository>();
					services.AddTransient<IPublishingOfficeRepository, PublishingOfficeRepository>();
					services.AddTransient<IAuthorRepository, AuthorRepository>();
					services.AddTransient<ICatalogItemRepository, CatalogItemRepository>();
					services.AddTransient<IBookRepository, BookRepository>();
					services.AddTransient<INewspaperRepository, NewspaperRepository>();
					services.AddTransient<IPatentRepository, PatentRepository>();

					services.AddValidatorsFromAssemblyContaining<BookValidator>();

					services.AddHostedService<Worker>();
				});
		}
		static void Main(string[] args)
        {
			CreateHostBuilder(args).Build().Run();
		}
    }
}
