﻿using Library.BLL.Validators;
using Library.Entities;
using NUnit.Framework;

namespace Library.BLL.Tests.Validators
{
    [TestFixture]
    public class AuthorValidatorTests
    {
        private static AuthorValidator _validator = new AuthorValidator();
       
        [TestCase("Author")]
        [TestCase("Александр")]
        [TestCase("Adam")]
        [TestCase("Анна-Мария")]
        [TestCase("Александрралександрралександрралександрралександрр")]
        [TestCase("О")]
        public void FirstName_IsValid(string firstName)
        {
            var author = GetValidAuthor();
            author.FirstName = firstName;
            var result = _validator.Validate(author);
            Assert.IsTrue(result.IsValid);
        }

        [TestCase("")]
        [TestCase(null)]
        [TestCase("александр")]
        [TestCase("Анна-")]
        [TestCase("-Мария")]
        [TestCase("Анна-мария")]
        [TestCase("Александрралександрралександрралександрралександррррррррр")]
        [TestCase("Иvan")]
        [TestCase("111")]
        [TestCase("Анна!Мария")]
        [TestCase("Анна Мария")]
        public void FirstName_IsNotValid(string firstName)
        {
            var author = GetValidAuthor();
            author.FirstName = firstName;
            var result = _validator.Validate(author);
            Assert.IsFalse(result.IsValid);
            Assert.AreEqual(nameof(Author.FirstName), result.Errors[0].PropertyName);
        }

        [TestCase("Тургенев")]
        [TestCase("фон Вейк")]
        [TestCase("ван Вейк")]
        [TestCase("О'Хара")]
        [TestCase("de Winter")]
        [TestCase("Мамин-Сибиряк")]
        [TestCase("Smith-Smith")]
        [TestCase("O'Harya")]
        [TestCase("Тургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневвв")]
        public void LastName_IsValid(string lastName)
        {
            var author = GetValidAuthor();
            author.LastName = lastName;
            var result = _validator.Validate(author);
            Assert.IsTrue(result.IsValid);
        }

        [TestCase("")]
        [TestCase(null)]
        [TestCase("Turгенев")]
        [TestCase("Тур1генев")]
        [TestCase("тургенев")]
        [TestCase("ван вейк")]
        [TestCase("'Охара")]
        [TestCase("Охара'")]
        [TestCase("-Охара")]
        [TestCase("Охара-")]
        [TestCase("de winter")]
        [TestCase("Мамин/Сибиряк")]
        [TestCase("smith-smith")]
        [TestCase("o'Harya")]
        [TestCase("Туургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневввтургеневвв")]
        public void LastName_IsNotValid(string lastName)
        {
            var author = GetValidAuthor();
            author.LastName = lastName;
            var result = _validator.Validate(author);
            Assert.IsFalse(result.IsValid);
        }

        private static Author GetValidAuthor()
        {
            return new Author
            {
                FirstName = "Authorname",
                LastName = "Authorlastname",
                Description = "Author Description",
                Id = 1,
            };
        }
    }
}