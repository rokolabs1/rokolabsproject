﻿using Library.BLL.Validators;
using NUnit.Framework;

namespace Library.BLL.Tests.Validators
{
    [TestFixture]
    public class ISSNValidatorTests
    {
        private static ISSNValidator _validator = new ISSNValidator();

        [TestCase("ISSN00-11-22-33")]
        [TestCase("ISSN99-88-77-66")]
        public void ISSNValidator_IsValid(string value)
        {
            Assert.IsTrue(_validator.Validate(value).IsValid);
        }

        [TestCase("ISSNBN-11-22-33")]
        [TestCase("ISSN9-88-77-66")]
        [TestCase("ISSN999-88-77-66")]
        [TestCase("ISSN9a-88-77-66")]
        [TestCase(" ISSN00-11-22-33")]
        [TestCase("ISSN00-11-22-33 ")]
        [TestCase("ISSN 00-11-22-33")]
        [TestCase("ISSN00-11 -22-33")]
        [TestCase("ISSN99-88-77-668")]
        public void ISSNValidator_IsNotValid(string value)
        {
            Assert.IsFalse(_validator.Validate(value).IsValid);
        }
    }

    [TestFixture]
    public class ISBNValidatorTests
    {
        private static ISBNValidator _validator = new ISBNValidator();

        [TestCase("ISBN ")]
        [TestCase("ISBN asdf")]
        [TestCase("ISBN 123")]
        public void StartsWithISBN_IsTrue(string value)
        {
            Assert.IsTrue(ISBNValidator.StartsWithISBN(value));
        }

        [TestCase("IDSB")]
        [TestCase("ISBN")]
        [TestCase("IDS B asdf")]
        [TestCase(" IDSB 123")]
        public void StartsWithISBN_IsFalse(string value)
        {
            Assert.IsFalse(ISBNValidator.StartsWithISBN(value));
        }

        [TestCase("1234567890")]
        [TestCase("123456sdf7890")]
        [TestCase("12-3-45-67-89-0")]
        [TestCase("123-45-6789-X")]
        public void HasCorrectDigitsCount_IsTrue(string value)
        {
            Assert.IsTrue(ISBNValidator.HasCorrectDigitsCount(value));
        }

        [TestCase("123456789")]
        [TestCase("12345678901")]
        [TestCase("12-3-45-67-89-00")]
        [TestCase("123-45-67890-X")]
        public void HasCorrectDigitsCount_IsFalse(string value)
        {
            Assert.IsFalse(ISBNValidator.HasCorrectDigitsCount(value));
        }

        [TestCase("0-0-0000000-0")]
        [TestCase("5-0-0000001-1")]
        [TestCase("7-0-0000022-2")]
        [TestCase("80-0-000333-3")]
        [TestCase("90-0-004444-4")]
        [TestCase("94-0-055555-5")]
        [TestCase("950-01-6666-6")]
        [TestCase("980-02-0000-7")]
        [TestCase("993-33-0001-8")]
        [TestCase("9940-444-22-9")]
        [TestCase("9980-555-22-X")]
        [TestCase("9989-666-33-0")]
        [TestCase("99900-777-4-1")]
        [TestCase("99980-888-5-2")]
        [TestCase("99999-999-6-3")]
        public void HasCorrectDigitGroups_IsTrue(string value)
        {
            Assert.IsTrue(ISBNValidator.HasCorrectDigitGroups(value));
        }

        [TestCase("8-0-0000022-2")]
        [TestCase("79-0-000333-3")]
        [TestCase("95-0-055555-5")]
        [TestCase("50-0-004444-4")]
        [TestCase("949-01-6666-6")]
        [TestCase("994-33-0001-8")]
        [TestCase("9939-444-22-9")]
        [TestCase("9990-666-33-0")]
        [TestCase("99899-777-4-1")]
        [TestCase("10000-999-6-3")]
        [TestCase("9980-555-22-Y")]
        [TestCase("9940-444-2-90")]
        public void HasCorrectDigitGroups_IsFalse(string value)
        {
            Assert.IsFalse(ISBNValidator.HasCorrectDigitGroups(value));
        }

        [TestCase("ISBN 0-0-0000000-0")]
        [TestCase("ISBN 5-0-0000001-1")]
        [TestCase("ISBN 7-0-0000022-2")]
        [TestCase("ISBN 80-0-000333-3")]
        [TestCase("ISBN 90-0-004444-4")]
        [TestCase("ISBN 94-0-055555-5")]
        [TestCase("ISBN 950-01-6666-6")]
        [TestCase("ISBN 980-02-0000-7")]
        [TestCase("ISBN 993-33-0001-8")]
        [TestCase("ISBN 9940-444-22-9")]
        [TestCase("ISBN 9980-555-22-X")]
        [TestCase("ISBN 9989-666-33-0")]
        [TestCase("ISBN 99900-777-4-1")]
        [TestCase("ISBN 99980-888-5-2")]
        [TestCase("ISBN 99999-999-6-3")]
        public void ISBNValidator_IsValid(string value)
        {
            Assert.IsTrue(_validator.Validate(value).IsValid);
        }

        [TestCase("ISBN  0-0-0000000-0")]
        [TestCase("ISBM 5-0-0000001-1")]
        [TestCase("ISBN 7-0-0-000022-2")]
        [TestCase(" ISBN 80-0-000333-3")]
        [TestCase("ISBN 900-004444-4X")]
        public void ISBNValidator_IsNotValid(string value)
        {
            Assert.IsFalse(_validator.Validate(value).IsValid);
        }
    }
}
