﻿using Library.BLL.Validators;
using Library.Entities;
using NUnit.Framework;

namespace Library.BLL.Tests.Validators
{
    [TestFixture]
    public class UserValidatorTests
    {
        private static UserValidator _validator = new UserValidator();

        [TestCase("IvAN1388")]
        [TestCase("ivan_1388")]
        [TestCase("IVAN_1388")]
        [TestCase("I1")]
        [TestCase("ivan_13_88")]

        public void Login_IsValid(string login)
        {
            var user = GetValidUser();
            user.Login = login;
            var result = _validator.Validate(user);
            Assert.IsTrue(result.IsValid);
        }

        [TestCase("")]
        [TestCase(null)]
        [TestCase("_Ivan")]
        [TestCase("ivan_")]
        [TestCase("ivan__89")]
        [TestCase("IVAN!&")]
        [TestCase("86ivan")]
        [TestCase("Иван12")]
        [TestCase("1112")]
        public void Login_IsNotValid(string login)
        {
            var user = GetValidUser();
            user.Login = login;
            var result = _validator.Validate(user);
            Assert.IsFalse(result.IsValid);
            Assert.AreEqual(nameof(User.Login), result.Errors[0].PropertyName);
        }

        [TestCase("Password")]
        [TestCase("PassWORD")]
        [TestCase("Пароль")]
        [TestCase("Pass8586!*&")]
        [TestCase("*&pass")]
        [TestCase("PAS")]
        [TestCase("12/pas-pass")]
        [TestCase("_88&па роль")]
        public void Password_IsValid(string password)
        {
            var user = GetValidUser();
            user.Password = password;
            var result = _validator.Validate(user);
            Assert.IsTrue(result.IsValid);
        }

        [TestCase("", null)]
        [TestCase(null, null)]
        [TestCase("pa", null)]
        [TestCase("ПАРОЛЬ", (char)8)]
        [TestCase("olgadiukarevapass88780496", null)]
        public void Password_IsNotValid(string password, char? additionalSymbol)
        {
            var user = GetValidUser();
            user.Password = password;
            if (additionalSymbol.HasValue)
            {
                user.Password += additionalSymbol.Value;
            }
            var result = _validator.Validate(user);
            Assert.IsFalse(result.IsValid);
        }

        private static User GetValidUser()
        {
            return new User
            {
                Login = "olgadiukareva",
                Password = "pass88780496",
            };
        }
    }
}