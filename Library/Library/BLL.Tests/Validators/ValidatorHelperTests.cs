using Library.BLL.Validators;
using NUnit.Framework;

namespace Library.BLL.Tests.Validators
{

    [TestFixture]
    public class ValidatorHelperTests
    {

        [TestCase("�van")]
        [TestCase("��on")]
        public void IsSingleLanguage_IsFalse(string value)
        {
            var result = ValidatorHelper.IsSingleLanguage(value);
            Assert.IsFalse(result);
        }

        [TestCase("����")]
        [TestCase("Jhon")]
        [TestCase("Adam")]
        [TestCase("���������")]
        [TestCase("")]
        [TestCase(null)]
        public void IsSingleLanguage_IsTrue(string value)
        {
            var result = ValidatorHelper.IsSingleLanguage(value);
            Assert.IsTrue(result);
        }

        [TestCase('a', "English")]
        [TestCase('A', "English")]
        [TestCase('z', "English")]
        [TestCase('Z', "English")]
        [TestCase('g', "English")]
        [TestCase('G', "English")]
        [TestCase('�', "Russian")]
        [TestCase('�', "Russian")]
        [TestCase('�', "Russian")]
        [TestCase('�', "Russian")]
        [TestCase('�', "Russian")]
        [TestCase('�', "Russian")]
        [TestCase('�', "Russian")]
        [TestCase(',', null)]
        [TestCase('1', null)]
        [TestCase('(', null)]
        [TestCase('\'', null)]
        [TestCase('-', null)]
        public void GetLanguageTests(char value, string expected)
        {
            var result = ValidatorHelper.GetLanguage(value);
            Assert.AreEqual(expected, result);
        }

        [TestCase("asdf")]
        [TestCase("as df")]
        [TestCase("as'df")]
        [TestCase("as-df")]
        [TestCase("as1df")]
        public void IsFirstAndLastSymbolsAreLetters_IsTrue(string value)
        {
            var result = ValidatorHelper.IsFirstAndLastSymbolsAreLetters(value);
            Assert.IsTrue(result);
        }

        [TestCase(" asdf")]
        [TestCase("asdf ")]
        [TestCase(" asdf ")]
        [TestCase("-asdf")]
        [TestCase("asdf-")]
        [TestCase("'asdf-")]
        [TestCase("")]
        [TestCase(null)]
        public void IsFirstAndLastSymbolsAreLetters_IsFalse(string value)
        {
            var result = ValidatorHelper.IsFirstAndLastSymbolsAreLetters(value);
            Assert.IsFalse(result);
        }

        [TestCase("asdf", new char[0])]
        [TestCase("asdf", null)]
        [TestCase("asdf", new [] {'\''})]
        [TestCase("as'df", new [] {'\''})]
        [TestCase("as'df", new [] {'\'', '-'})]
        [TestCase("as'd-f", new [] {'\'', '-'})]
        [TestCase("a s'd-f", new [] {'\'', '-', ' '})]
        public void IsLettersOrAllowedChars_IsTrue(string value, char[] allowedChars)
        {
            var result = ValidatorHelper.IsLettersOrAllowedChars(value, allowedChars);
            Assert.IsTrue(result);
        }


        [TestCase(null, new char[0])]
        [TestCase("", new char[0])]
        [TestCase("as'df", new char[0])]
        [TestCase("asd1f", null)]
        [TestCase("asd f", new[] { '\'' })]
        [TestCase("as'd f", new[] { '\'' })]
        [TestCase("as'd-f,", new[] { '\'', '-' })]
        public void IsLettersOrAllowedChars_IsFalse(string value, char[] allowedChars)
        {
            var result = ValidatorHelper.IsLettersOrAllowedChars(value, allowedChars);
            Assert.IsFalse(result);
        }

        [TestCase("Asdf", null)]
        [TestCase("Asdf", new string[0])]
        [TestCase("As Df", new string[] { " " })]
        [TestCase("As-Df", new string[] { "-" })]
        [TestCase("As Df-Gh", new string[] { " ", "-" })]
        [TestCase("As �� Df", new string[] { "�� " })]
        public void IsUpperAfterSymbols_IsTrue(string value, string[] separators)
        {
            var result = ValidatorHelper.IsUpperAfterSeparators(value, separators);
            Assert.IsTrue(result);
        }

        [TestCase(null, null)]
        [TestCase("", null)]
        [TestCase("asdf", null)]
        [TestCase("asdf", new string[0])]
        [TestCase("As df", new string[] { " " })]
        [TestCase("As-df", new string[] { "-" })]
        [TestCase("As df-gh", new string[] { " ", "-" })]
        [TestCase("As �� df", new string[] { "�� " })]
        public void IsUpperAfterSymbols_IsFalse(string value, string[] separators)
        {
            var result = ValidatorHelper.IsUpperAfterSeparators(value, separators);
            Assert.IsFalse(result);
        }
    }
}