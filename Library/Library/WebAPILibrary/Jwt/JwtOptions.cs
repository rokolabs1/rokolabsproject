﻿using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace WebAPILibrary.Jwt
{
    public class JwtOptions
    {
        public string Issuer { get; set; }
        public string Audience { get;set;}
        public string Key { get; set; }
        public int LifetimeMinutes { get; set; }

        public SymmetricSecurityKey GetSymmetricSecurityKey()
        {
            return new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Key));
        }
    }
}
