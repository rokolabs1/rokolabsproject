﻿using Library.Entities;

namespace WebAPILibrary.Jwt
{
    public interface ITokenService
    {
        string BuildToken(User user);
    }
}
