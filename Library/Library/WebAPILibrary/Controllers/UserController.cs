﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPILibrary.Models;

namespace WebAPILibrary.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "admin")]
    public class UserController : ControllerBase
    {
        private readonly ILogger<UserController> _logger;
        private readonly IUserService _userService;
        private readonly IMapper _mapper;

        public UserController(ILogger<UserController> logger, IUserService userService, IMapper mapper)
        {
            _logger = logger;
            _userService = userService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserModel>>> Get()
        {
            try
            {
                var users = _mapper.Map<List<UserModel>>(_userService.GetUsers());
                return users;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when getting users.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<UserModel>> Get(int id)
        {
            try
            {
                User user = _userService.GetUserById(id);
                if (user == null)
                {
                    return NotFound();
                }

                var userCatalog = _mapper.Map<UserModel>(user);
                return Ok(userCatalog);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when getting a user.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult<UserModel>> Post([FromBody] NewUserModel model)
        {
            try
            {
                if (model.Password != model.ConfirmPassword)
                {
                    return BadRequest("Password and confirm password are different.");
                }

                var user = _mapper.Map<UserModel>(_userService.Register(_mapper.Map<User>(model)));
                return CreatedAtAction(nameof(Get), new { id = user.Id }, model);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors.Select(x => new { x.PropertyName, x.ErrorMessage }));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when creating a new user.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPatch("{id}/role")]
        public async Task<ActionResult> Patch(int id, [FromBody] string role)
        {
            try
            {
                User user = _userService.GetUserById(id);
                if (user == null)
                {
                    return NotFound();
                }
                user.Role = role;
                _userService.ChangeUserRole(user);
                return Accepted();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when editing a user role.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
