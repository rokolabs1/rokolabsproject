﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPILibrary.Models;

namespace WebAPILibrary.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class AuthorController : ControllerBase
    {
        private readonly ILogger<AuthorController> _logger;
        private readonly IAuthorService _authorService;
        private readonly IMapper _mapper;

        public AuthorController(IAuthorService authorService, IMapper mapper, ILogger<AuthorController> logger)
        {
            _authorService = authorService;
            _mapper = mapper;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<AuthorModel>>> Get()
        {
            try
            {
                var items = _mapper.Map<IEnumerable<AuthorModel>>(_authorService.GetAuthors());
                return Ok(items);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when getting authors.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<AuthorModel>> Get(int id)
        {
            try
            {
                var item = _mapper.Map<AuthorModel>(_authorService.GetAuthorsByIds(new[] { id }).FirstOrDefault());
                if (item == null)
                {
                    return NotFound();
                }

                return Ok(item);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when getting an author.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        public async Task<ActionResult<AuthorModel>> Post([FromBody] AuthorModel model)
        {
            try
            {
                model.Id = _authorService.InsertAuthor(_mapper.Map<Author>(model));
                return CreatedAtAction(nameof(Get), new { id = model.Id }, model);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors.Select(x => new { x.PropertyName, x.ErrorMessage }));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when creating a new author.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] AuthorModel model)
        {
            try
            {
                var existedItem = _authorService.GetAuthorsByIds(new[] { id }).FirstOrDefault();
                if (existedItem == null)
                {
                    return NotFound();
                }

                model.Id = id;
                _authorService.UpdateAuthor(id, _mapper.Map<Author>(model));
                return Accepted();
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors.Select(x => new { x.PropertyName, x.ErrorMessage }));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when updating thr author.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var existedItem = _authorService.GetAuthorsByIds(new[] { id }).FirstOrDefault();
                if (existedItem == null)
                {
                    return NotFound();
                }

                _authorService.DeleteAuthor(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when deleting thr author.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
