﻿ using AutoMapper;
using Library.BLL.Interfaces;
using Library.DAL;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WebAPILibrary.Models;

namespace WebAPILibrary.Controllers
{

    [Authorize(Roles = "admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class CatalogController : ControllerBase
    {
        private readonly ILogger<CatalogController> _logger;
        private readonly ICatalogService _catalogService;
        private readonly IMapper _mapper;

        public CatalogController(ICatalogService catalogService, IMapper mapper, ILogger<CatalogController> logger)
        {
            _catalogService = catalogService;
            _mapper = mapper;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CatalogItemModel>>> Get(int skip = 0, int count = 10)
        {
            try
            {
                var items = _mapper.Map<IEnumerable<CatalogItemModel>>(_catalogService.GetCatalogItems(skip, count));
                return Ok(items);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when getting catalog items.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [AllowAnonymous]
        [HttpGet("count")]
        public async Task<ActionResult<int>> GetCount()
        {
            try
            {
                return Ok(_catalogService.GetCatalogItemsCount());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when getting catalog items count.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}/type")]
        public async Task<ActionResult<string>> Get(int id)
        {
            try
            {
                var type = _catalogService.GetTypeById(id);
                if (type == CatalogItemType.Unknown)
                {
                    return NotFound();
                }

                return Ok(type.ToString());
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when getting a catalog item type. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var type = _catalogService.GetTypeById(id);
                if (type == CatalogItemType.Unknown)
                {
                    return NotFound();
                }

                _catalogService.DeleteItem(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when deleting the catalog item. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
