﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPILibrary.Models;

namespace WebAPILibrary.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class NewspaperController : ControllerBase
    {
        private readonly ILogger<NewspaperController> _logger;
        private readonly INewspaperService _newspaperService;
        private readonly IMapper _mapper;

        public NewspaperController(INewspaperService newspaperService, IMapper mapper, ILogger<NewspaperController> logger)
        {
            _newspaperService = newspaperService;
            _mapper = mapper;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<NewspaperModel>>> Get()
        {
            try
            {
                var items = _mapper.Map<IEnumerable<NewspaperModel>>(_newspaperService.GetNewspapers());
                return Ok(items);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when getting newspapers.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<NewspaperModel>> Get(int id)
        {
            try
            {
                var item = _mapper.Map<NewspaperModel>(_newspaperService.GetNewspapers(new[] { id }).FirstOrDefault());
                if (item == null)
                {
                    return NotFound();
                }

                return Ok(item);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when getting a newspaper. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}/issues")]
        public async Task<ActionResult<IEnumerable<NewspaperIssueModel>>> GetIssues(int id)
        {
            try
            {
                var item = _mapper.Map<NewspaperModel>(_newspaperService.GetNewspapers(new[] { id }).FirstOrDefault());
                if (item == null)
                {
                    return NotFound();
                }

                var items = _mapper.Map<IEnumerable<NewspaperIssueModel>>(_newspaperService.GetNewspaperIssues(id));

                return Ok(items);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when getting a newspaper issues. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        public async Task<ActionResult<NewspaperModel>> Post([FromBody] NewspaperModel model)
        {
            try
            {
                model.Id = _newspaperService.InsertNewspaper(_mapper.Map<Newspaper>(model));
                return CreatedAtAction(nameof(Get), new { id = model.Id }, model);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors.Select(x => new { x.PropertyName, x.ErrorMessage }));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when creating a new newspaper.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] NewspaperModel model)
        {
            try
            {
                var existedItem = _newspaperService.GetNewspapers(new[] { id }).FirstOrDefault();
                if (existedItem == null)
                {
                    return NotFound();
                }

                model.Id = id;
                _newspaperService.UpdateNewspaper(_mapper.Map<Newspaper>(model));
                return Accepted();
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors.Select(x => new { x.PropertyName, x.ErrorMessage }));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when updating the newspaper. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var existedItem = _newspaperService.GetNewspapers(new[] { id }).FirstOrDefault();
                if (existedItem == null)
                {
                    return NotFound();
                }

                _newspaperService.DeleteNewspaper(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when deleting the newspaper. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
