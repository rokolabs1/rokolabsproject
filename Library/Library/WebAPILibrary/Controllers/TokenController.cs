﻿using Microsoft.AspNetCore.Mvc;
using WebAPILibrary.Jwt;
using Library.BLL.Interfaces;

namespace TokenApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TokenController : ControllerBase
    {
        private readonly ITokenService _tokenService;
        private readonly IUserService _userService;
        public TokenController(ITokenService tokenService, IUserService userService)
        {
            _tokenService = tokenService;
            _userService = userService;
        }

        [HttpPost("/token")]
        public IActionResult Token(string username, string password)
        {
            var user = _userService.Login(new Library.Entities.User
            {
                Login = username,
                Password = password
            });
            if (user == null)
            {
                return BadRequest(new { errorText = "Invalid username or password." });
            }

            return Ok(_tokenService.BuildToken(user));
        }
    }
}