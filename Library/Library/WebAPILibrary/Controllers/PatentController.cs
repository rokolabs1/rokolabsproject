﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPILibrary.Models;

namespace WebAPILibrary.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class PatentController : ControllerBase
    {
        private readonly ILogger<PatentController> _logger;
        private readonly IPatentService _patentService;
        private readonly IMapper _mapper;

        public PatentController(IPatentService patentService, IMapper mapper, ILogger<PatentController> logger)
        {
            _patentService = patentService;
            _mapper = mapper;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<PatentModel>>> Get()
        {
            try
            {
                var items = _mapper.Map<IEnumerable<PatentModel>>(_patentService.GetPatents());
                return Ok(items);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when getting patents.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<PatentModel>> Get(int id)
        {
            try
            {
                var author = _mapper.Map<PatentModel>(_patentService.GetPatents(new[] { id }).FirstOrDefault());
                if (author == null)
                {
                    return NotFound();
                }

                return Ok(author);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when getting a patent. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        public async Task<ActionResult<PatentModel>> Post([FromBody] PatentModel model)
        {
            try
            {
                model.Id = _patentService.InsertPatent(_mapper.Map<Patent>(model));
                return CreatedAtAction(nameof(Get), new { id = model.Id }, model);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors.Select(x => new { x.PropertyName, x.ErrorMessage }));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when creating a new patent.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] PatentModel model)
        {
            try
            {
                var existedItem = _patentService.GetPatents(new[] { id }).FirstOrDefault();
                if (existedItem == null)
                {
                    return NotFound();
                }

                model.Id = id;
                _patentService.UpdatePatent(_mapper.Map<Patent>(model));
                return Accepted();
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors.Select(x => new { x.PropertyName, x.ErrorMessage }));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when updating the patent. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var existedItem = _patentService.GetPatents(new[] { id }).FirstOrDefault();
                if (existedItem == null)
                {
                    return NotFound();
                }

                _patentService.DeletePatent(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when deleting the patent. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
