﻿using AutoMapper;
using FluentValidation;
using Library.BLL.Interfaces;
using Library.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using WebAPILibrary.Models;

namespace WebAPILibrary.Controllers
{
    [Authorize(Roles = "admin")]
    [Route("api/[controller]")]
    [ApiController]
    public class BookController : ControllerBase
    {
        private readonly ILogger<BookController> _logger;
        private readonly IBookService _bookService;
        private readonly IMapper _mapper;

        public BookController(IBookService bookService, IMapper mapper, ILogger<BookController> logger)
        {
            _bookService = bookService;
            _mapper = mapper;
            _logger = logger;
        }

        [AllowAnonymous]
        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookModel>>> Get()
        {
            try
            {
                var items = _mapper.Map<IEnumerable<BookModel>>(_bookService.GetBooks());
                return Ok(items);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when getting books.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [AllowAnonymous]
        [HttpGet("{id}")]
        public async Task<ActionResult<BookModel>> Get(int id)
        {
            try
            {
                var item = _mapper.Map<BookModel>(_bookService.GetBooks(new[] { id }).FirstOrDefault());
                if (item == null)
                {
                    return NotFound();
                }

                return Ok(item);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when getting a book. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPost]
        public async Task<ActionResult<BookModel>> Post([FromBody] BookModel model)
        {
            try
            {
                model.Id = _bookService.InsertBook(_mapper.Map<Book>(model));
                return CreatedAtAction(nameof(Get), new { id = model.Id }, model);
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors.Select(x => new { x.PropertyName, x.ErrorMessage }));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Error when creating a new book.");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> Put(int id, [FromBody] BookModel model)
        {
            try
            {
                var existedItem = _bookService.GetBooks(new[] { id }).FirstOrDefault();
                if (existedItem == null)
                {
                    return NotFound();
                }

                model.Id = id;
                _bookService.UpdateBook(_mapper.Map<Book>(model));
                return Accepted();
            }
            catch (ValidationException ex)
            {
                return BadRequest(ex.Errors.Select(x => new { x.PropertyName, x.ErrorMessage }));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when updating the book. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> Delete(int id)
        {
            try
            {
                var existedItem = _bookService.GetBooks(new[] { id }).FirstOrDefault();
                if (existedItem == null)
                {
                    return NotFound();
                }

                _bookService.DeleteBook(id);
                return Ok();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Error when deleting the book. Id: {id}");
                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }
    }
}
