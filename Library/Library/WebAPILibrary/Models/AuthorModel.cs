﻿using System.ComponentModel.DataAnnotations;

namespace WebAPILibrary.Models
{
    public class AuthorModel
    {
        public int Id { get; set; }
        [Required]
        public string FirstName { get; set; }
        [Required]
        public string LastName { get; set; }
        public string Description { get; set; }
    }
}
