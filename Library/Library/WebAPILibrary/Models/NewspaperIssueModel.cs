﻿namespace WebAPILibrary.Models
{
    public class NewspaperIssueModel
    {
        public int Id { get; set; }
        public int Number { get; set; }
    }
}