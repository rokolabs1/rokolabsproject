﻿using System;
using System.ComponentModel.DataAnnotations;

namespace WebAPILibrary.Models
{
    public class NewspaperModel
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public int Number { get; set; }
        [Required]
        public short PageCount { get; set; }
        [Required]
        public string City { get; set; }
        public string ISSN { get; set; }
        [Required]
        public int PublicationYear { get; set; }

        [Required]
        [DataType(DataType.Date)]
        public DateTime PublicationDate { get; set; }

        [Required]
        public string PublishingOffice { get; set; }
        public string Description { get; set; }
    }
}
