﻿namespace WebAPILibrary.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string Role { get; set; }
        public string Login { get; set; }
    }
}
