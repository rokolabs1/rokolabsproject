﻿namespace WebAPILibrary.Models
{
    public class CatalogItemModel
    {
        public int CatalogItemId { get; set; }
        public string Name { get; set; }
        public string PublicId { get; set; }
        public int PageCount { get; set; }
    }
}
