﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace WebAPILibrary.Models
{
    public class PatentModel
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public short PageCount { get; set; }

        [Required]
        public string Country { get; set; }

        [Required]
        public int NumberRegistration { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime PublicationDate { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime ApplicationDate { get; set; }
        [Required]
        public List<int> Authors { get; set; }
        public string Description { get; set; }
    }
}
