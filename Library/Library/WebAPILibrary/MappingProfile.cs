﻿using AutoMapper;
using Library.DAL.DTO;
using Library.Entities;
using System.Linq;
using WebAPILibrary.Models;

namespace WebAPILibrary
{
    public class MappingProfile : Profile
    {
        public MappingProfile()
        {
            CreateMap<City, CityDTO>();
            CreateMap<CityDTO, City>();

            CreateMap<Country, CountryDTO>();
            CreateMap<CountryDTO, Country>();

            CreateMap<PublishingOffice, PublishingOfficeDTO>();
            CreateMap<PublishingOfficeDTO, PublishingOffice>();

            CreateMap<Author, AuthorDTO>();
            CreateMap<AuthorDTO, Author>();

            CreateMap<User, UserDTO>();
            CreateMap<UserDTO, User>();

            CreateMap<CatalogItemViewDTO, CatalogItem>()
                .ForMember(obj => obj.CatalogItemId, opt => opt.MapFrom(obj => obj.Id));

            CreateMap<Book, BookDTO>()
                .ForMember(obj => obj.CityId, opt => opt.MapFrom(obj => obj.City.Id))
                .ForMember(obj => obj.PublishingOfficeId, opt => opt.MapFrom(obj => obj.PublishingOffice.Id))
                .ForMember(obj => obj.AuthorIds, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.Id).ToList()));
            CreateMap<BookDTO, Book>();

            CreateMap<Patent, PatentDTO>()
                .ForMember(obj => obj.CountryId, opt => opt.MapFrom(obj => obj.Country.Id))
                .ForMember(obj => obj.AuthorIds, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.Id).ToList()));
            CreateMap<PatentDTO, Patent>()
                .ForPath(obj => obj.Country.Id, opt => opt.MapFrom(obj => obj.CountryId))
                .ForMember(obj => obj.Authors, opt => opt.MapFrom(obj => obj.AuthorIds.Select(a => new Author { Id = a }).ToList()));

            CreateMap<Newspaper, NewspaperDTO>()
                .ForMember(obj => obj.CityId, opt => opt.MapFrom(obj => obj.City.Id))
                .ForMember(obj => obj.PublishingOfficeId, opt => opt.MapFrom(obj => obj.PublishingOffice.Id));
            CreateMap<NewspaperDTO, Newspaper>()
                .ForPath(obj => obj.City.Id, opt => opt.MapFrom(obj => obj.CityId))
                .ForPath(obj => obj.PublishingOffice.Id, opt => opt.MapFrom(obj => obj.PublishingOfficeId));


            CreateMap<Book, BookModel>()
                .ForMember(obj => obj.PublishingOffice, opt => opt.MapFrom(obj => obj.PublishingOffice.Name))
                .ForMember(obj => obj.City, opt => opt.MapFrom(obj => obj.City.Name))
                .ForMember(obj => obj.Authors, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.Id).ToList()));
            CreateMap<BookModel, Book>()
                .ForPath(obj => obj.PublishingOffice.Name, opt => opt.MapFrom(obj => obj.PublishingOffice))
                .ForPath(obj => obj.City.Name, opt => opt.MapFrom(obj => obj.City))
                .ForMember(obj => obj.Authors, opt => opt.MapFrom(obj => obj.Authors.Select(id => new Author { Id = id }).ToList()));

            CreateMap<Patent, PatentModel>()
                .ForMember(obj => obj.Country, opt => opt.MapFrom(obj => obj.Country.Name))
                .ForMember(obj => obj.Authors, opt => opt.MapFrom(obj => obj.Authors.Select(a => a.Id).ToList()));
            CreateMap<PatentModel, Patent>()
                .ForPath(obj => obj.Country.Name, opt => opt.MapFrom(obj => obj.Country))
                .ForMember(obj => obj.Authors, opt => opt.MapFrom(obj => obj.Authors.Select(id => new Author { Id = id }).ToList()));

            CreateMap<Newspaper, NewspaperModel>()
                .ForMember(obj => obj.PublishingOffice, opt => opt.MapFrom(obj => obj.PublishingOffice.Name))
                .ForMember(obj => obj.City, opt => opt.MapFrom(obj => obj.City.Name));
            CreateMap<NewspaperModel, Newspaper>()
               .ForMember(obj => obj.PublishingOffice, opt => opt.MapFrom(obj => new PublishingOffice { Name = obj.PublishingOffice }))
               .ForMember(obj => obj.City, opt => opt.MapFrom(obj => new City { Name = obj.City }));
            CreateMap<Newspaper, NewspaperIssueModel>();


            CreateMap<CatalogItem, CatalogItemModel>()
                .ForMember(obj => obj.CatalogItemId, opt => opt.MapFrom(obj => obj.CatalogItemId));

            CreateMap<NewUserModel, User>();

            CreateMap<User, UserModel>();
            CreateMap<UserModel, User>();

            CreateMap<Author, AuthorModel>();
            CreateMap<AuthorModel, Author>();
        }
    }
}
