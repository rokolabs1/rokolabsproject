﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Threading.Tasks;

namespace WebAPILibrary.Middlewares
{
    public class LogEntity
    {
        public string Message { get; set; }
        public string UserName { get; set; }
        public string MethodInfo { get; set; }
        public string ClassName { get; set; }
    }

    public class ExceptionHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ExceptionHandlerMiddleware> _logger;

        public ExceptionHandlerMiddleware(RequestDelegate next, ILogger<ExceptionHandlerMiddleware> logger)
        {
            _logger = logger;
            _next = next;
        }

        public async Task InvokeAsync(HttpContext context)
        {
            try
            {
                await _next.Invoke(context);
                var endpoint = context.GetEndpoint();
                if (endpoint != null)
                {
                    var logEntity = new LogEntity
                    {
                        MethodInfo = endpoint.DisplayName,
                        UserName = context.User.Identity.Name,
                    };

                    if (context.Response.StatusCode == StatusCodes.Status403Forbidden && context.User.Identity.IsAuthenticated)
                    {
                        logEntity.Message = "Unauthorized access";
                        _logger.LogWarning("{@LogEntity}", logEntity);
                    }
                    else if (context.User.IsInRole("admin"))
                    {
                        logEntity.Message = "Admin action";
                        _logger.LogInformation("{@LogEntity}", logEntity);
                    }
                }
            }
            catch (System.Exception ex)
            {
                var logEntity = new LogEntity
                {
                    Message = $"Unhandled exception. {ex.Message}",
                    UserName = context.User.Identity.Name,
                    MethodInfo = ex.TargetSite.Name,
                    ClassName = ex.TargetSite.DeclaringType.FullName,
                };
                _logger.LogError(ex, "{@LogEntity}", logEntity);
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
            }
        }
    }
}
