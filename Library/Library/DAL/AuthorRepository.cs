﻿using Library.DAL.Interfaces;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using Dapper;
using System.Linq;
using Library.DAL.DTO;
using Library.DAL.Helpers;

namespace Library.DAL
{
	public class AuthorRepository : IAuthorRepository
	{
		private readonly ILogger<AuthorRepository> _logger;
		private readonly DBOptions _options;
		public AuthorRepository(ILogger<AuthorRepository> logger, DBOptions options)
		{
			_logger = logger;
			_options = options;
		}

		public void DeleteAuthor(int id)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = @"delete from [Authors] where [Id] = @id";
				db.Execute(query, new { id });
			}
		}

		public List<AuthorDTO> GetAuthors()
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "SELECT * FROM [Authors]";
				return db.Query<AuthorDTO>(query).ToList();
			}
		}

		public List<AuthorDTO> GetAuthorsByIds(IEnumerable<int> ids)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "SELECT * FROM [Authors] a inner join @ids i on a.Id = i.EntityId;";
				return db.Query<AuthorDTO>(query, new { ids = ids.AsDtIntEntity() }).ToList();
			}
		}

		public int InsertAuthor(AuthorDTO author)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var existed = GetExistedAuthor(author, db, false);
				if (existed != null)
                {
					_logger.LogWarning($"InsertAuthor: Attempt to insert already existed author: FirstName '{author.FirstName}', LastName '{author.LastName}', Description '{author.Description}'. " +
						$"Existed id '{existed.Id}'.");
					return existed.Id;
                }

				var query = "INSERT INTO [Authors] ([FirstName], [LastName], Description) VALUES (@firstname, @lastname, @description); SELECT CAST(SCOPE_IDENTITY() as int);";
				return db.Query<int>(query, author).FirstOrDefault();
			}
		}

		public bool UpdateAuthor(int authorId, AuthorDTO author)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var existed = GetExistedAuthor(author, db, true);
				if (existed != null)
				{
					_logger.LogWarning($"UpdateAuthor: Attempt to update author with existed properties: FirstName '{author.FirstName}', LastName '{author.LastName}', Description '{author.Description}'. " +
						$"Existed id '{author.Id}'.");
					return false;
				}

				var query = "UPDATE [Authors] SET [FirstName] = @firstname, [LastName] = @lastname, Description = @description WHERE [Id] = @Id";
				db.Execute(query, author);
				return true;
			}
		}

		private AuthorDTO GetExistedAuthor(AuthorDTO author, IDbConnection connection, bool forUpdate)
        {
			var query = @"
select * from Authors
where FirstName = @firstname and LastName = @lastname
and ((Description is null and @description is null) or Description = @description)";
			if (forUpdate)
            {
				query += " and Id <> @id";
            }
			return connection.QueryFirstOrDefault<AuthorDTO>(query, author);
        }
	}
}
