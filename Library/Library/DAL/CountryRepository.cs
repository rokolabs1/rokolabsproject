﻿using Dapper;
using Library.DAL.DTO;
using Library.DAL.Helpers;
using Library.DAL.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Library.DAL
{
    public class CountryRepository: ICountryRepository
    {
		private readonly ILogger<CountryRepository> _logger;
		private readonly DBOptions _options;
		public CountryRepository(ILogger<CountryRepository> logger, DBOptions options)
		{
			_logger = logger;
			_options = options;
		}

		public int InsertIfNotExists(CountryDTO country)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = @"
if exists (select * from [Countries] c where c.Name = @name)
begin
	select Id from [Countries] c where c.Name = @name;
end
else
begin
	insert into [Countries] ([Name]) values (@name);
	select cast (scope_identity() as int);
end;";
				return db.Query<int>(query, country).FirstOrDefault();
			}
		}

		public List<CountryDTO> Get(IEnumerable<int> ids)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "select Id, [Name] from [Countries] o inner join @ids i on o.Id = i.EntityId;";
				return db.Query<CountryDTO>(query, new { ids = ids.AsDtIntEntity() }).ToList();
			}
		}
	}
}
