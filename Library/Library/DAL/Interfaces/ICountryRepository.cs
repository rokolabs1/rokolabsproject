﻿using Library.DAL.DTO;
using System.Collections.Generic;

namespace Library.DAL.Interfaces
{
    public interface ICountryRepository
    {
        int InsertIfNotExists(CountryDTO country);
        List<CountryDTO> Get(IEnumerable<int> ids);
    }
}
