﻿using Library.DAL.DTO;
using System.Collections.Generic;

namespace Library.DAL.Interfaces
{
	public interface IAuthorRepository
	{
		void DeleteAuthor(int id);
		int InsertAuthor(AuthorDTO author);
		bool UpdateAuthor(int id, AuthorDTO author);
		List<AuthorDTO> GetAuthors();
		List<AuthorDTO> GetAuthorsByIds(IEnumerable<int> ids);
	}
}
