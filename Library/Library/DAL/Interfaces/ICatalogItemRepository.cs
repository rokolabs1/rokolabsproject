﻿using Library.DAL.DTO;
using System.Collections.Generic;

namespace Library.DAL.Interfaces
{
    public interface ICatalogItemRepository
    {
        CatalogItemDTO Get(int id);
        int Insert(CatalogItemDTO dto);
        void DeleteItem(int id);
        List<CatalogItemViewDTO> GetCatalogItemViews(int skip, int count);
        int GetCatalogItemsCount();
    }
}
