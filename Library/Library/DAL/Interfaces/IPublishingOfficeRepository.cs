﻿using Library.DAL.DTO;
using System.Collections.Generic;

namespace Library.DAL.Interfaces
{
    public interface IPublishingOfficeRepository
    {
        int InsertIfNotExists(PublishingOfficeDTO publishingOffice);
        List<PublishingOfficeDTO> Get(IEnumerable<int> ids);
    }
}
