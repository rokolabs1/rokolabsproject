﻿using Library.DAL.DTO;
using System.Collections.Generic;

namespace Library.DAL.Interfaces
{
    public interface IUserRepository
    {
        UserDTO Login(UserDTO user);
        int Register(UserDTO user);
        List<UserDTO> GetUsers();
        UserDTO GetUserById(int id);
        void ChangeUserRole(UserDTO user);
    }
}
