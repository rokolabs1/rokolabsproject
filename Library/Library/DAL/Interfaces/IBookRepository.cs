﻿using Library.DAL.DTO;
using System.Collections.Generic;

namespace Library.DAL.Interfaces
{
	public interface IBookRepository
	{
		void DeleteBook(int id);
		void InsertBook(BookDTO book);
		bool UpdateBook(int id, BookDTO book);
		List<BookDTO> GetBooks(IEnumerable<int> ids = null);
	}
}
