﻿using Library.DAL.DTO;
using System.Collections.Generic;

namespace Library.DAL.Interfaces
{
    public interface ICityRepository
    {
        int InsertIfNotExists(CityDTO city);
        List<CityDTO> Get(IEnumerable<int> ids);
    }
}
