﻿using Library.DAL.DTO;
using System.Collections.Generic;

namespace Library.DAL.Interfaces
{
    public interface IPatentRepository
    {
        void DeletePatent(int id);
        void InsertPatent(PatentDTO patent);
        void UpdatePatent(int id, PatentDTO patent);
        List<PatentDTO> GetPatents(IEnumerable<int> ids = null);
    }
}
