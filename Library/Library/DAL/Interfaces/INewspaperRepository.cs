﻿using Library.DAL.DTO;
using System.Collections.Generic;


namespace Library.DAL.Interfaces
{
    public interface INewspaperRepository
    {
        void DeleteNewspaper(int id);
        int InsertNewspaper(NewspaperDTO newspaper);
        void UpdateNewspaper(int id, NewspaperDTO newspaper);
        List<NewspaperDTO> GetNewspapers(IEnumerable<int> ids = null);
        List<NewspaperDTO> GetNewspaperIssues(int id);
    }
}
