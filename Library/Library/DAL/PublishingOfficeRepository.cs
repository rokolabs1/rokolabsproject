﻿using Dapper;
using Library.DAL.DTO;
using Library.DAL.Helpers;
using Library.DAL.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Library.DAL
{
    public class PublishingOfficeRepository: IPublishingOfficeRepository
    {
		private readonly ILogger<PublishingOfficeRepository> _logger;
		private readonly DBOptions _options;
		public PublishingOfficeRepository(ILogger<PublishingOfficeRepository> logger, DBOptions options)
		{
			_logger = logger;
			_options = options;
		}

		public int InsertIfNotExists(PublishingOfficeDTO publishingOffice)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = @"
if exists (select * from [PublishingOffices] c where c.Name = @name)
begin
	select Id from [PublishingOffices] c where c.Name = @name;
end
else
begin
	insert into [PublishingOffices] ([Name]) values (@name);
	select cast (scope_identity() as int);
end;";
				return db.Query<int>(query, publishingOffice).FirstOrDefault();
			}
		}

		public List<PublishingOfficeDTO> Get(IEnumerable<int> ids)
        {
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
				var query = "select Id, [Name] from PublishingOffices o inner join @ids i on o.Id = i.EntityId;";
				return db.Query<PublishingOfficeDTO>(query, new { ids = ids.AsDtIntEntity() }).ToList();
			}
		}
	}
}
