﻿using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Dapper;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using Library.DAL.Helpers;

namespace Library.DAL
{
    public class PatentRepository : IPatentRepository
    {
		private readonly ILogger<PatentRepository> _logger;
		private readonly DBOptions _options;
		public PatentRepository(ILogger<PatentRepository> logger, DBOptions options)
		{
			_logger = logger;
			_options = options;
		}

		public List<PatentDTO> GetPatents(IEnumerable<int> ids = null)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
                string query;
                List<PatentDTO> patents;
                if (ids == null)
                {
                    query = "select * from [Patents]";
                    patents = db.Query<PatentDTO>(query).ToList();
                }
                else
                {
                    query = "select p.* from [Patents] p inner join @ids i on p.Id = i.EntityId;";
                    patents = db.Query<PatentDTO>(query, new { ids = ids.AsDtIntEntity() }).ToList();
                }

                var authorsQuery = "select aci.* from [AuthorsCatalogItems] aci inner join @ids i on aci.CatalogItemId = i.EntityId;";
                var authors = db.Query<AuthorCatalogItemDTO>(authorsQuery, new { ids = patents.Select(b => b.Id).AsDtIntEntity() });

                foreach (var patent in patents)
                {
                    patent.AuthorIds = authors.Where(a => a.CatalogItemId == patent.Id).Select(a => a.AuthorId).ToList();
                }

                return patents;
            }
		}
		public void DeletePatent(int id)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "DELETE FROM [Patents] WHERE [Id] = @id";
				db.Execute(query, new { id });
			}
		}

        public void InsertPatent(PatentDTO patent)
        {
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{

                var authorsQuery = @"
insert into AuthorsCatalogItems (AuthorId, CatalogItemId)
select i.EntityId, @patentId from @ids i;";
                db.Execute(authorsQuery, new { patentId = patent.Id, ids = patent.AuthorIds.AsDtIntEntity() });

                var query = @"
INSERT INTO [Patents] (Id, [Title], [PageCount], [CountryId], [PublicationDate], [ApplicationDate], [NumberRegistration], [Description])
VALUES (@id, @title, @pageCount, @countryId, @publicationDate, @applicationDate, @numberRegistration, @description);";
                db.Execute(query, patent);
            }
		}

        public void UpdatePatent(int id, PatentDTO patent)
        {
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
                var authorsQuery = @"
delete from AuthorsCatalogItems where CatalogItemId = @patentId;
insert into AuthorsCatalogItems (AuthorId, CatalogItemId)
select i.EntityId, @patentId from @ids i;";
                db.Execute(authorsQuery, new { patentId = patent.Id, ids = patent.AuthorIds.AsDtIntEntity() });

                var query = @"
UPDATE [Patents] 
SET [Title] = @title, [PageCount] = @pageCount, 
[CountryId] = @countryId, [NumberRegistration] = @numberRegistration, 
[PublicationDate] = @publicationDate, [ApplicationDate] = @applicationDate, [Description] = @description
WHERE [Id] = @id";
                db.Execute(query, patent);
            }
		}
    }
}
