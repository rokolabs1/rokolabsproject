﻿using Dapper;
using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using Microsoft.Data.SqlClient;
using System.Linq;
using Library.DAL.Helpers;

namespace Library.DAL
{
    public class BookRepository : IBookRepository
    {
        private readonly ILogger<BookRepository> _logger;
        private readonly DBOptions _options;

        public BookRepository(ILogger<BookRepository> logger, DBOptions options)
        {
            _logger = logger;
            _options = options;
        }
        public void DeleteBook(int id)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "DELETE FROM [Books] WHERE [Id] = @id";
                db.Execute(query, new { id });
            }
        }

        public void InsertBook(BookDTO book)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var existed = GetExistedBook(book, db, false);
                if (existed.HasValue)
                {
                    _logger.LogWarning($"InsertBook: Attempt to insert already existed book: ISBN '{book.ISBN}', Title '{book.Title}', PublishingYear '{book.PublishingYear}', Authors '{string.Join(", ", book.AuthorIds)}'. " +
                        $"Existed id '{existed}'.");
                    return;
                }

                var authorsQuery = @"
insert into AuthorsCatalogItems (AuthorId, CatalogItemId)
select i.EntityId, @bookId from @ids i;";
                db.Execute(authorsQuery, new { bookId = book.Id, ids = book.AuthorIds.AsDtIntEntity() });

                var query = @"
INSERT INTO [Books] (Id, [Title], [PageCount], [CityId], [PublishingYear], [ISBN], [PublishingOfficeId], [Description])
VALUES (@id, @title, @pageCount, @cityId, @publishingYear, @ISBN, @publishingOfficeId, @description);";
                db.Execute(query, book);
            }
        }

        public List<BookDTO> GetBooks(IEnumerable<int> ids = null)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                string query;
                List<BookDTO> books;
                if(ids == null)
                {
                    query = "select * from [Books]";
                    books = db.Query<BookDTO>(query).ToList();
                }
                else
                {
                    query = "select b.* from [Books] b inner join @ids i on b.Id = i.EntityId;";
                    books = db.Query<BookDTO>(query, new { ids = ids.AsDtIntEntity() }).ToList();
                }

                var authorsQuery = "select aci.* from [AuthorsCatalogItems] aci inner join @ids i on aci.CatalogItemId = i.EntityId;";
                var authors = db.Query<AuthorCatalogItemDTO>(authorsQuery, new { ids = books.Select(b => b.Id).AsDtIntEntity() });

                foreach (var book in books)
                {
                    book.AuthorIds = authors.Where(a => a.CatalogItemId == book.Id).Select(a => a.AuthorId).ToList();
                }

                return books;
            }
        }

        public bool UpdateBook(int id, BookDTO book)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var existed = GetExistedBook(book, db, true);
                if (existed.HasValue)
                {
                    _logger.LogWarning($"UpdateBook: Attempt to update book with existed properties: ISBN '{book.ISBN}', Title '{book.Title}', PublishingYear '{book.PublishingYear}', Authors '{string.Join(", ", book.AuthorIds)}'. " +
                        $"Existed id '{existed}'.");
                    return false;
                }

                var authorsQuery = @"
delete from AuthorsCatalogItems where CatalogItemId = @bookId;
insert into AuthorsCatalogItems (AuthorId, CatalogItemId)
select i.EntityId, @bookId from @ids i;";
                db.Execute(authorsQuery, new { bookId = book.Id, ids = book.AuthorIds.AsDtIntEntity() });

                var query = @"
UPDATE [Books]
SET [Title] = @title, [PageCount] = @pageCount, [CityId] = @cityId, [PublishingYear] = @publishingYear,
[ISBN] = @ISBN, [PublishingOfficeId] = @publishingOfficeId, [Description] = @description
WHERE [Id] = @id";
                db.Execute(query, book);
                return true;
            }
        }

        private int? GetExistedBook(BookDTO book, IDbConnection connection, bool forUpdate)
        {
            string query;
            if (!string.IsNullOrWhiteSpace(book.ISBN))
            {
                query = "select * from Books b where ISBN = @isbn";
                if (forUpdate)
                {
                    query += " and b.Id <> @bookId";
                }

                return connection.QueryFirstOrDefault<BookDTO>(query, new
                {
                    bookId = book.Id,
                    isbn = book.ISBN,
                })?.Id;
            }

            query = @"
select AuthorId, CatalogItemId from [Books] b 
inner join [AuthorsCatalogItems] aci on b.Id = aci.CatalogItemId
where Title = @title and PublishingYear = @publishingYear";

            if (forUpdate)
            {
                query += " and aci.CatalogItemId <> @bookId";
            }

            var authorCatalogItems = connection.Query<AuthorCatalogItemDTO>(
                query,
                new
                {
                    title = book.Title,
                    publishingYear = book.PublishingYear,
                    bookId = book.Id
                }).ToList();

            var authorsGroupedByBook = authorCatalogItems.GroupBy(
                a => a.CatalogItemId,
                (bookId, authorIds) => new
                {
                    BookId = bookId,
                    AuthorIds = authorIds.Select(obj => obj.AuthorId).ToList()
                });

            foreach (var item in authorsGroupedByBook)
            {
                if (item.AuthorIds.Count == book.AuthorIds.Count && item.AuthorIds.Except(book.AuthorIds).Any())
                {
                    return item.BookId;
                }
            }

            return null;
        }
    }
}
