﻿namespace Library.DAL
{
    public enum CatalogItemType
    {
        Unknown = 0,
        Book = 1,
        Newspaper = 2,
        Patent = 3,
    }
}
