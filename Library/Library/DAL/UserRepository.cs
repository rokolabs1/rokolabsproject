﻿using Dapper;
using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Library.DAL
{
    public class UserRepository : IUserRepository
    {
        private readonly ILogger<UserRepository> _logger;
        private readonly DBOptions _options;

        public UserRepository(ILogger<UserRepository> logger, DBOptions options)
        {
            _logger = logger;
            _options = options;
        }

        public List<UserDTO> GetUsers()
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT Id, FirstName, LastName, [Login], [Password], [Role] FROM [Users]";
                return db.Query<UserDTO>(query).ToList();
            }
        }
        public void ChangeUserRole(UserDTO user)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = @"
UPDATE[Users]
SET[Role] = @role
WHERE[Id] = @id;";
               db.Execute(query, user);
            }
        }

        public UserDTO Login(UserDTO user)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT Id, FirstName, LastName, [Login], [Password], [Role] FROM [Users] WHERE Login = @login and Password = @password;";
                return db.QueryFirstOrDefault<UserDTO>(query, user);
            }
        }

        public int Register(UserDTO user)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                if(GetExistedUser(user, db))
                {
                    throw new Exception("User with this login already exists.");
                }
                var query = "INSERT INTO [Users] (Login, Password, Role) VALUES (@login, @password, @role); SELECT CAST(SCOPE_IDENTITY() as int);";
                return db.ExecuteScalar<int>(query, user);
            }
        }

        private bool GetExistedUser(UserDTO user, IDbConnection db)
        {
            var query = "SELECT COUNT ([Login]) FROM [Users] WHERE Login = @login;";
            return db.ExecuteScalar<int>(query, user) > 0;
        }

        public UserDTO GetUserById(int id)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "SELECT Id, FirstName, LastName, [Login], [Password], [Role] FROM [Users] WHERE Id = @id";
                return db.Query<UserDTO>(query, new { id }).FirstOrDefault();
            }
        }
    }
}
