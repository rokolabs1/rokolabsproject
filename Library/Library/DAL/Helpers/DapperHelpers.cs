﻿using System.Collections.Generic;
using System.Data;
using System.Linq;
using Dapper;

namespace Library.DAL.Helpers
{
    internal static class DapperHelpers
    {
        public static SqlMapper.ICustomQueryParameter AsDtIntEntity(this IEnumerable<int> ids)
        {
			DataTable dt = new DataTable();
			dt.Columns.Add("EntityID");
			foreach (var id in ids.Distinct())
			{
				dt.Rows.Add(id);
			}

			return dt.AsTableValuedParameter("dtIntEntity");
		}
    }
}
