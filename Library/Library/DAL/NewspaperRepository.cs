﻿using Library.DAL.Interfaces;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Data.SqlClient;
using System.Data;
using Library.DAL.DTO;
using Microsoft.Extensions.Logging;
using Dapper;
using Library.DAL.Helpers;

namespace Library.DAL
{
    public class NewspaperRepository : INewspaperRepository
    {
		private readonly ILogger<NewspaperRepository> _logger;
		private readonly DBOptions _options;
		public NewspaperRepository(ILogger<NewspaperRepository> logger, DBOptions options)
		{
			_logger = logger;
			_options = options;
		}

        public List<NewspaperDTO> GetNewspaperIssues(int id)
        {
            var newspaper = GetNewspapers(new[] { id }).FirstOrDefault();
            string query;
            if (string.IsNullOrEmpty(newspaper.ISSN))
            {
                query = "select * from [Newspapers] where Title = @title and PublishingOfficeId = @publishingOfficeId order by PublicationDate desc;";
            }
            else
            {
                query = "select * from [Newspapers] where ISSN =@issn order by PublicationDate desc;";
            }
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                return db.Query<NewspaperDTO>(query, new { title = newspaper.Title, publishingOfficeId = newspaper.PublishingOfficeId, issn = newspaper.ISSN }).ToList();
            }
        }
        public void DeleteNewspaper(int id)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "DELETE FROM [Newspapers] WHERE [Id] = @id";
				db.Execute(query, new { id });
			}
		}

		public List<NewspaperDTO> GetNewspapers(IEnumerable<int> ids = null)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
                string query;
                List<NewspaperDTO> newspapers;
                if (ids == null)
                {
                    query = "select * from [Newspapers]";
                    newspapers = db.Query<NewspaperDTO>(query).ToList();
                }
                else
                {
                    query = "select b.* from [Newspapers] b inner join @ids i on b.Id = i.EntityId;";
                    newspapers = db.Query<NewspaperDTO>(query, new { ids = ids.AsDtIntEntity() }).ToList();
                }
                return newspapers;
            }
        }

        public int InsertNewspaper(NewspaperDTO newspaper)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = @"
                 INSERT INTO [Newspapers] ([Id], [Title], [PageCount], [CityId], [Number], [ISSN], [PublicationYear], [PublicationDate], [PublishingOfficeId], [Description]) VALUES (@id, @title, @pageCount, @cityId, @number, @ISSN, @publicationYear, @publicationDate, @publishingOfficeId, @description);";
                return db.Query<int>(query, newspaper).FirstOrDefault();
            }
        }

        public void UpdateNewspaper(int id, NewspaperDTO newspaper)
        {
            using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
                var query = "UPDATE [Newspapers] SET [Title] = @title, [PageCount] = @pageCount, [CityId] = @cityId, [Number] = @number, [ISSN] = @ISSN, [PublicationYear] = @publicationYear, [PublicationDate] = @publicationDate, [PublishingOfficeId] = @publishingOfficeId, [Description] = @description WHERE [Id] = @id";
                db.Execute(query, newspaper);
            }
        }
    }
}
