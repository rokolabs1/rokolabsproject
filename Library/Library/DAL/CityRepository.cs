﻿using Dapper;
using Library.DAL.DTO;
using Library.DAL.Helpers;
using Library.DAL.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Library.DAL
{
    public class CityRepository: ICityRepository
    {
		private readonly ILogger<CityRepository> _logger;
		private readonly DBOptions _options;
		public CityRepository(ILogger<CityRepository> logger, DBOptions options)
		{
			_logger = logger;
			_options = options;
		}

        public int InsertIfNotExists(CityDTO city)
        {
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = @"
if exists (select * from Cities c where c.Name = @name)
begin
	select Id from Cities c where c.Name = @name;
end
else
begin
	insert into Cities ([Name]) values (@name);
	select cast (scope_identity() as int);
end;";
				return db.Query<int>(query, city).FirstOrDefault();
			}
		}

		public List<CityDTO> Get(IEnumerable<int> ids)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "select Id, [Name] from [Cities] o inner join @ids i on o.Id = i.EntityId;";
				return db.Query<CityDTO>(query, new { ids = ids.AsDtIntEntity() }).ToList();
			}
		}
	}
}
