﻿using Dapper;
using Library.DAL.DTO;
using Library.DAL.Interfaces;
using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using System.Linq;

namespace Library.DAL
{
    public class CatalogItemRepository: ICatalogItemRepository
	{
		private readonly ILogger<CatalogItemRepository> _logger;
		private readonly DBOptions _options;
		public CatalogItemRepository(ILogger<CatalogItemRepository> logger, DBOptions options)
		{
			_logger = logger;
			_options = options;
		}

		public CatalogItemDTO Get(int id)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "SELECT * FROM [CatalogItems] WHERE [Id] = @id";
				return db.Query<CatalogItemDTO>(query, new { id = id }).FirstOrDefault();
			}
		}
		public void DeleteItem(int id)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "DELETE FROM [CatalogItems] WHERE [Id] = @id;";
				db.Execute(query, new { id = id });
			}
		}

		public int Insert(CatalogItemDTO dto)
        {
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
				var query = "insert into CatalogItems ([Type]) values (@type); SELECT CAST(SCOPE_IDENTITY() as int);";
				return db.Query<int>(query, dto).FirstOrDefault();
			}
		}

		public List<CatalogItemViewDTO> GetCatalogItemViews(int skip, int count)
        {
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
            {
				var query = "SELECT * FROM CatalogItemsView ORDER BY[Name] OFFSET @skip ROWS FETCH NEXT @count ROWS ONLY;";
				return db.Query<CatalogItemViewDTO>(query, new { skip, count }).ToList();
            }
		}

		public int GetCatalogItemsCount()
        {
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "SELECT count(*) FROM CatalogItemsView;";
				return db.ExecuteScalar<int>(query);
			}
		}
	}
}
