﻿namespace Library.DAL.DTO
{
    public class AuthorCatalogItemDTO
    {
		public int Id { get; set; }
		public int AuthorId { get; set; }
		public int CatalogItemId { get; set; }
	}
}
