﻿namespace Library.DAL.DTO
{
    public class PublishingOfficeDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
