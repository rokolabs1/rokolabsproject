﻿namespace Library.DAL
{
    public class CatalogItemDTO
    {
        public int Id { get; set; }
        public CatalogItemType Type { get; set; }
    }
}
