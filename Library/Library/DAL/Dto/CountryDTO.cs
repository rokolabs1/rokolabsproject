﻿namespace Library.DAL.DTO
{
    public class CountryDTO
    {
        public int Id;
        public string Name { get; set; }
    }
}
