﻿using System;

namespace Library.DAL.DTO
{
    public class NewspaperDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public int? Number { get; set; }
        public short PageCount { get; set; }
        public int CityId { get; set; }
        public string ISSN { get; set; }
        public int PublicationYear { get; set; }
        public DateTime PublicationDate { get; set; }
        public int PublishingOfficeId { get; set; }
        public string Description { get; set; }
    }
}
