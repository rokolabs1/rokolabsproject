﻿using System;
using System.Collections.Generic;


namespace Library.DAL.DTO
{
    public class PatentDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public short PageCount { get; set; }
        public int CountryId { get; set; }
        public int NumberRegistration { get; set; }
        public DateTime PublicationDate { get; set; }
        public DateTime ApplicationDate { get; set; }
        public string Description { get; set; }
        public List<int> AuthorIds { get; set; }
    }
}
