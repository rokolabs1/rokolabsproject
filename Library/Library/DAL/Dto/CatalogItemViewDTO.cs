﻿namespace Library.DAL.DTO
{
    public class CatalogItemViewDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int PageCount { get; set; }
        public string PublicId { get; set; }
    }
}
