﻿using System.Collections.Generic;

namespace Library.DAL.DTO
{
	public class BookDTO 
	{
		public int Id { get; set; }
		public string Title { get; set; }
		public short PageCount { get; set; }
        public int CityId { get; set; }
        public int PublishingYear { get; set; }
		public string ISBN { get; set; }
		public int PublishingOfficeId { get; set; }
		public string Description { get; set; }
		public List<int> AuthorIds { get; set; }
	}
}
