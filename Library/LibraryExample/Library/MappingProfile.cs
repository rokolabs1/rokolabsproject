﻿using AutoMapper;
using Library.Dal.Dto;
using Library.Entities;

namespace Library
{
	public class MappingProfile : Profile
	{
		public MappingProfile()
		{
			CreateMap<Author, AuthorDto>();
			CreateMap<AuthorDto, Author>();
			CreateMap<Book, BookDto>();
			CreateMap<BookDto, Book>();
		}
	}
}
