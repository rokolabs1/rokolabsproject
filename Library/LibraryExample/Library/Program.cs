﻿using AutoMapper;
using Library.Bll;
using Library.Bll.Interfaces;
using Library.Dal;
using Library.Dal.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;

namespace Library
{
	class Program
	{
		static void Main(string[] args)
		{
			CreateHostBuilder(args).Build().Run();
		}

		private static IHostBuilder CreateHostBuilder(string[] args)
		{
			return Host.CreateDefaultBuilder(args)
				.ConfigureServices((hostingContext, services) =>
				{
					IConfiguration configuration = hostingContext.Configuration;
					var connectionString = configuration.GetConnectionString("DefaultConnection");

					var mappingConfig = new MapperConfiguration(mc =>
					{
						mc.AddProfile(new MappingProfile());
					});

					IMapper mapper = mappingConfig.CreateMapper();
					services.AddSingleton(mapper);

					services.AddSingleton(new DBOptions() { ConnectionString = connectionString });
					services.AddTransient<IAuthorService, AuthorService>();
					services.AddTransient<IAuthorRepository, AuthorRepository>();
					services.AddTransient<IBookService, BookService>();
					services.AddTransient<IBookRepository, BookRepository>();

					services.AddHostedService<Worker>();
				});
		}
	}
}
