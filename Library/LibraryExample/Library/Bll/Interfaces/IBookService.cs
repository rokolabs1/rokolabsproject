﻿using Library.Entities;
using System.Collections.Generic;

namespace Library.Bll.Interfaces
{
	public interface IBookService
	{
		List<Book> GetBooks();
	}
}
