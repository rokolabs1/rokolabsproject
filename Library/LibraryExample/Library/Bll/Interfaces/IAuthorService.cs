﻿using Library.Entities;
using System.Collections.Generic;

namespace Library.Bll.Interfaces
{
	public interface IAuthorService
	{
		List<Author> GetAuthors();
		int InsertAuthor(Author author);
		void UpdateAuthor(int id, Author author);
		void DeleteAuthor(int id);
	}
}
