﻿using AutoMapper;
using Library.Bll.Interfaces;
using Library.Dal.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Linq;

namespace Library.Bll
{
	public class BookService : IBookService
	{
		private readonly ILogger<BookService> _logger;
		private readonly IBookRepository _bookRepository;
		private readonly IAuthorRepository _authorRepository;
		private readonly IMapper _mapper;
		public BookService(ILogger<BookService> logger, IMapper mapper, IBookRepository bookRepository, IAuthorRepository authorRepository)
		{
			_logger = logger;
			_bookRepository = bookRepository;
			_authorRepository = authorRepository;
			_mapper = mapper;
		}

		public List<Book> GetBooks()
		{
			var books = _mapper.Map<List<Book>>(_bookRepository.GetBooks());
			var authors = _authorRepository.GetAuthorsByIds(books.Select(p => p.BookId));
			foreach (var book in books)
			{
				book.Authors = _mapper.Map<List<Author>>(authors.Where(p => book.BookId == p.BookId));
			}
			return books;
		}
	}
}
