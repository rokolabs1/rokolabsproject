﻿using AutoMapper;
using Library.Bll.Interfaces;
using Library.Dal.Dto;
using Library.Dal.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;

namespace Library.Bll
{
	public class AuthorService : IAuthorService
	{
		private readonly ILogger<AuthorService> _logger;
		private readonly IAuthorRepository _authorRepository;
		private readonly IMapper _mapper;
		public AuthorService(ILogger<AuthorService> logger, IMapper mapper, IAuthorRepository authorRepository)
		{
			_logger = logger;
			_authorRepository = authorRepository;
			_mapper = mapper;
		}

		public void DeleteAuthor(int id)
		{
			_authorRepository.DeleteAuthor(id);
		}

		public List<Author> GetAuthors()
		{
			return _mapper.Map<List<Author>>(_authorRepository.GetAuthors());
		}

		public int InsertAuthor(Author author)
		{
			return _authorRepository.InsertAuthor(_mapper.Map<AuthorDto>(author));
		}

		public void UpdateAuthor(int id, Author author)
		{
			_authorRepository.UpdateAuthor(id, _mapper.Map<AuthorDto>(author));
		}
	}
}
