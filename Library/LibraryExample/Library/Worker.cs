﻿using Library.Bll.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Library
{
	public class Worker : BackgroundService
	{
		private readonly ILogger<Worker> _logger;
		private readonly IAuthorService _authorService;
		private readonly IBookService _bookService;

		public Worker(ILogger<Worker> logger, IAuthorService authorService, IBookService bookService)
		{
			_logger = logger;
			_authorService = authorService;
			_bookService = bookService;
		}

		protected override Task ExecuteAsync(CancellationToken stoppingToken)
		{
			List<Author> authors = _authorService.GetAuthors();

			ShowAuthors(authors);

			int id = _authorService.InsertAuthor(new Author() { FirstName = "New", LastName = "Author" });

			authors = _authorService.GetAuthors();
			ShowAuthors(authors);

			_authorService.UpdateAuthor(id, new Author() { AuthorId = id, FirstName = "Updated", LastName = "Author" });

			authors = _authorService.GetAuthors();
			ShowAuthors(authors);

			_authorService.DeleteAuthor(id);
			authors = _authorService.GetAuthors();
			ShowAuthors(authors);

			List<Book> books = _bookService.GetBooks();
			ShowBooks(books);

			return Task.CompletedTask;
		}

		private static void ShowAuthors(List<Author> authors)
		{
			foreach (var author in authors)
			{
				Console.WriteLine($"{author.AuthorId} {author.FirstName} {author.LastName}");
			}
			Console.WriteLine();
		}

		private static void ShowBooks(List<Book> books)
		{
			foreach (var book in books)
			{
				Console.WriteLine($"{book.BookId} {book.Title}");
				ShowAuthors(book.Authors);
			}
			Console.WriteLine();
		}
	}
}