﻿using Dapper;
using Library.Dal.Dto;
using Library.Dal.Interfaces;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Library.Dal
{
	public class BookRepository : IBookRepository
	{
		private readonly ILogger<BookRepository> _logger;
		private readonly DBOptions _options;
		public BookRepository(ILogger<BookRepository> logger, DBOptions options)
		{
			_logger = logger;
			_options = options;
		}

		public List<BookDto> GetBooks()
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "SELECT * FROM [Books]";
				return db.Query<BookDto>(query).ToList();
			}
		}
	}
}
