﻿using Library.Dal.Interfaces;
using Library.Entities;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using Dapper;
using System.Linq;
using Library.Dal.Dto;

namespace Library.Dal
{
	public class AuthorRepository : IAuthorRepository
	{
		private readonly ILogger<AuthorRepository> _logger;
		private readonly DBOptions _options;
		public AuthorRepository(ILogger<AuthorRepository> logger, DBOptions options)
		{
			_logger = logger;
			_options = options;
		}

		public void DeleteAuthor(int id)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "DELETE FROM [Authors] WHERE [AuthorId] = @id";
				db.Execute(query, new { id });
			}
		}

		public List<AuthorDto> GetAuthors()
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "SELECT * FROM [Authors]";
				return db.Query<AuthorDto>(query).ToList();
			}
		}

		public List<AuthorDto> GetAuthorsByIds(IEnumerable<int> ids)
		{
			DataTable dt = new DataTable();
			dt.Columns.Add("EntityID");
			foreach (var id in ids)
			{
				dt.Rows.Add(id);
			}

			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				return db.Query<AuthorDto>("GetAuthotsByBookIds", 
					new { ids = dt.AsTableValuedParameter("dtIntEntity") },
					commandType: CommandType.StoredProcedure
					).ToList();
			}
		}

		public int InsertAuthor(AuthorDto author)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "INSERT INTO [Authors] ([FirstName], [LastName]) VALUES (@firstname, @lastname); SELECT CAST(SCOPE_IDENTITY() as int);";
				return db.Query<int>(query, author).FirstOrDefault();
			}
		}

		public void UpdateAuthor(int authorId, AuthorDto author)
		{
			using (IDbConnection db = new SqlConnection(_options.ConnectionString))
			{
				var query = "UPDATE [Authors] SET [FirstName] = @firstname, [LastName] = @lastname WHERE [AuthorId] = @authorId";
				db.Execute(query, author);
			}
		}
	}
}
