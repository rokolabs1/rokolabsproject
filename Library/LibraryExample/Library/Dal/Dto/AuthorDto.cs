﻿namespace Library.Dal.Dto
{
	public class AuthorDto
	{
		public int AuthorId { get; set; }
		public int? BookId { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }
	}
}
