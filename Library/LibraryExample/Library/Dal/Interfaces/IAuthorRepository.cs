﻿using Library.Dal.Dto;
using Library.Entities;
using System.Collections.Generic;

namespace Library.Dal.Interfaces
{
	public interface IAuthorRepository
	{
		List<AuthorDto> GetAuthors();
		void DeleteAuthor(int id);
		int InsertAuthor(AuthorDto author);
		void UpdateAuthor(int id, AuthorDto author);
		List<AuthorDto> GetAuthorsByIds(IEnumerable<int> ids);
	}
}
