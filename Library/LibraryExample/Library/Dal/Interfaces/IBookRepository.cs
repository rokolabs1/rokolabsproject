﻿using Library.Dal.Dto;
using System.Collections.Generic;

namespace Library.Dal.Interfaces
{
	public interface IBookRepository
	{
		List<BookDto> GetBooks();
	}
}
