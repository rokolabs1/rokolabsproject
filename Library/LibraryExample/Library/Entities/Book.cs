﻿using System.Collections.Generic;

namespace Library.Entities
{
	public class Book
	{
		public int BookId { get; set; }
		public string Title { get; set; }
		public string Description { get; set; }
		public List<Author> Authors { get; set; }
	}
}
