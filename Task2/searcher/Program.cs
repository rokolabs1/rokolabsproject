﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using System.Linq;

namespace searcher
{
    public class Program
    {
        public static void GetInstruction()
        {
            Console.WriteLine($"The sequence of passing arguments when starting the application:{Environment.NewLine}" +
            $"1. Folder name; {Environment.NewLine}" +
            $"2. File type {Environment.NewLine}" +
            $"Keys for the command:{Environment.NewLine}" +
            $"-l - key to for output to the console and to the log file; {Environment.NewLine}" +
            $"-r - key to search by regex; {Environment.NewLine}" +
            "-h or -help or no arguments - key for instructions.");
        }
        public static void WriteInLogFile(string nameLogFile, List<FileResultInfo> results)
        {
            using (StreamWriter writer = new StreamWriter(nameLogFile))
            {
                int number = 1;
                foreach (FileResultInfo result in results)
                {
                    writer.WriteLine($"File: {result.Name} \n");
                    foreach (ItemInfo info in result.Items)
                    {
                        writer.WriteLine($"{number}.String: {info.StringNumber}, Position: {info.Position}, Result: {info.Result} \n");
                        number++;
                    }
                }
            }
        }
        public static void Print(List<FileResultInfo> results)
        {
            int number = 1;
            foreach (FileResultInfo result in results)
            {
                Console.WriteLine($"File: {result.Name} \n");
                foreach (ItemInfo info in result.Items)
                {
                    Console.WriteLine($"{number}.String: {info.StringNumber}, Position: {info.Position}, Result: {info.Result} \n");
                    number++;
                }
            }
        }
        public static List<ItemInfo> SearchText(string line, string textForSearch, int numberLine)
        {
            int position = line.IndexOf(textForSearch);
            List<ItemInfo> items = new List<ItemInfo>();
            while (position != -1)
            {
                ItemInfo item = new ItemInfo(numberLine + 1, position, line);
                items.Add(item);
                position = line.IndexOf(textForSearch, position + 1);
            }
            return items;
        }
        public static List<ItemInfo> SearchRegex(string line, string regexForSearch, int numberLine)
        {
            List<ItemInfo> items = new List<ItemInfo>();
            MatchCollection matches = Regex.Matches(line, regexForSearch);

            foreach (Match match in matches)
            {
                ItemInfo item = new ItemInfo(numberLine + 1, match.Index, line);
                items.Add(item);
            }
            return items;
        }
        public static List<FileResultInfo> GetResult(IEnumerable<String> files, string textForSearch, string regexForSearch)
        {
            List<FileResultInfo> results = new List<FileResultInfo>();
            foreach (var file in files)
            {
                List<string> stringsfile = new List<string>();
                using (StreamReader reader = new StreamReader(file))
                {
                    for (string line = reader.ReadLine(); line != null; line = reader.ReadLine())
                    {
                        stringsfile.Add(line);
                    }
                }

                List<ItemInfo> items = new List<ItemInfo>();
                for (int i = 0; i < stringsfile.Count; i++)
                {
                    if (textForSearch != null)
                    {
                        items.AddRange(SearchText(stringsfile[i], textForSearch, i));
                    }
                    else
                    {
                        items.AddRange(SearchRegex(stringsfile[i], regexForSearch, i));
                    }
                }
                if (items.Count > 0)
                {
                    results.Add(new FileResultInfo(file, items));
                }
            }
            return results;
        }
        public static void Main(string[] args)
        {
            if (args.Length == 0 || args.Contains("-h") || args.Contains("-help") || args.Length < 3)
            {
                GetInstruction();
            }
            else
            {
                string nameFolder = args[0];
                string typeFile = args[1];
                string textForSearch = null;
                string regexForSearch = null;
                string nameLogFile = null;
                for (int i = 2; i < args.Length; i++)
                {
                    if (args[i] == "-r")
                    {
                        if (i + 1 != args.Length)
                        {
                            regexForSearch = args[i + 1];
                        }
                        else
                        {
                            textForSearch = "-r";
                        }
                    }
                }
                try
                {
                    if (!Directory.Exists(nameFolder))
                    {
                        Console.WriteLine("Wrong directory name.");
                        return;
                    }
                    IEnumerable<String> files = Directory.EnumerateFiles(nameFolder, typeFile, SearchOption.AllDirectories);


                    Print((GetResult(files, textForSearch, regexForSearch)));
                    if (args.Contains("-l"))
                    {
                        nameLogFile = args[Array.IndexOf(args, "-l") + 1];
                        WriteInLogFile(nameLogFile, GetResult(files, textForSearch, regexForSearch));
                    }
                }
                catch(Exception e)
                {
                    Console.WriteLine($"Something went wrong: {e.Message}");
                }

            }
        }
    }
}
