﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace searcher
{
    public class ItemInfo
    {
        public int StringNumber { get; set; }
        public int Position { get; set; }
        public string Result { get; set; }
        public ItemInfo(int stringNumber, int position, string result)
        {
            StringNumber = stringNumber;
            Position = position;
            Result = result;
        }
    }
}
