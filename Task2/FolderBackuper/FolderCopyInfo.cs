﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FolderBackuper
{
    public class FolderCopyInfo
    {
        public DateTime TimeStamp { get; set; }
        public List<FileCopyInfo> Files { get; set; }
        public FolderCopyInfo(DateTime timeStamp, List<FileCopyInfo> files)
        {
            TimeStamp = timeStamp;
            Files = files;
        }
        public static List<FolderCopyInfo> GetFolderCopyInfos(string[] logLines)
        {
            List<FolderCopyInfo> folderCopyInfos = new List<FolderCopyInfo>();
            string fileID = null;
            foreach (string item in logLines)
            {
                DateTime result;
                if (DateTime.TryParse(item, out result))
                {
                    FolderCopyInfo folderCopy = new FolderCopyInfo(result, new List<FileCopyInfo>());
                    folderCopyInfos.Add(folderCopy);
                    continue;
                }
                if (fileID == null)
                {
                    fileID = item;
                }
                else
                {
                    folderCopyInfos[folderCopyInfos.Count - 1].Files.Add(new FileCopyInfo(fileID, item));
                    fileID = null;
                }
            }
            return folderCopyInfos;
        }
    }
}
