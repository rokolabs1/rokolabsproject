﻿using System.Collections.Generic;

namespace Game
{
    public class Level
    {
        public int Levelid { get; set; }
        public int Width { get; set; }
        public int Height { get; set; }
        public int LevelNumber { get; set; } = 1;
        public List<PlayingElementSettings> Elements { get; set; }
  
        public Level()
        {
            Elements = new List<PlayingElementSettings>();
        }
    }
}
