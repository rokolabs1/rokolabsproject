﻿using System;

namespace Game
{
    public class PlayingElementSettings
    {
        public Types  PlayingElementType { get; set; }
        public int PlayingElementQuantity { get; set; }
        public PlayingElementSettings(Types type, int quantity)
        {
            PlayingElementType = type;
            PlayingElementQuantity = quantity;
        }
    }
}
