﻿using Game.DTOs; 

namespace Game
{
    public abstract class PlayingElement
    {
        public string Name { get; set; }
        public int CoordinateX { get; set; }
        public int CoordinateY { get; set; }
        protected PlayingElement(int x, int y)
        {
            CoordinateX = x;
            CoordinateY = y;
        }
        protected PlayingElement(ElementState state)
        {
            CoordinateX = state.X;
            CoordinateY = state.Y;
        }
        public abstract void Draw();
        public abstract Types GetElementType();
        public virtual ElementState GetState()
        {
            ElementState state = new ElementState(GetElementType(), CoordinateX, CoordinateY);
            return state;
        }
    }
}
