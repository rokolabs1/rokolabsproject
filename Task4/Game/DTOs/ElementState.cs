﻿


namespace Game.DTOs
{
    public class ElementState
    {
        public Types Type { get; set; }
        public int X { get; set; }
        public int Y { get; set; }
        public int Health { get; set; }
        public int ImpactOnHelth { get; set; }
        public int NumberOfPassesThroughObstacles { get; set; }
        public ElementState(Types type, int x, int y)
        {
            Type = type;
            X = x;
            Y = y;
        }
    }
}
