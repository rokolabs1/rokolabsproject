﻿using System;
using Game.DTOs;


namespace Game
{
    public class Blueberry: PlayingElement, IBonus
    {
        public Blueberry(int x, int y)
         : base(x, y)
        {
            Name = "Blueberry";
        }
        public Blueberry(ElementState state)
         : base(state)
        {
            Name = "Blueberry";
        }
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.Write("B");
        }
        public override Types GetElementType()
        {
            return Types.Blueberry;
        }
    }
}
