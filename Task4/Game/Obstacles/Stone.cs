﻿using System;
using Game.DTOs;

namespace Game
{
    public class Stone : PlayingElement, ISurmountabilityBlock
    {
        public Stone(int x, int y)
            : base(x, y)
        {
            Name = "Stone";
        }
        public Stone(ElementState state)
         : base(state)
        {
            Name = "Stone";
        }

        public bool SurmountabilityBlock(PlayingElement element)
        {
            return false;
        }
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("S");
        }
        public override Types GetElementType()
        {
            return Types.Stone;
        }
    }
}
