﻿using System;
using Game.DTOs;

namespace Game
{
    public class Wall : PlayingElement, ISurmountabilityBlock
    {
        public Wall(int x, int y)
            : base(x, y)
        {
            Name = "Wall";
        }
        public Wall(ElementState state)
         : base(state)
        {
            Name = "Wall";
        }

        public bool SurmountabilityBlock(PlayingElement element)
        {
            return false;
        }
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write("W");
        }
        public override Types GetElementType()
        {
            return Types.Wall;
        }
    }
}
