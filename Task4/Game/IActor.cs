﻿namespace Game
{
    public interface IActor
    {
        void DoStep();
    }
}
