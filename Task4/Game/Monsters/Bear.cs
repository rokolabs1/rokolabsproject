﻿using System;
using Game.DTOs;


namespace Game
{
    public class Bear : Monster, IActor, IImpactOnHelth
    {
        public Bear(PlayingField field, int x, int y)
            : base(field, x, y)
        {
            Name = "Bear";
        }
        public Bear(PlayingField field, ElementState state)
           : base(field, state)
        {
             Name = "Bear";
            _impactOnHelth = state.ImpactOnHelth;
        }
        private int _impactOnHelth = -30;
        public override int ImpactOnHelth
        {
            get
            {
                return _impactOnHelth;
            }
        }
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("B");
        }
        protected override void InteractWithBonuses(int newX, int newY)
        {
            base.InteractWithBonuses(newX, newY);
            Raspberries raspberries = _field.GetElement(newX, newY) as Raspberries;
            if (raspberries != null)
            {
                _impactOnHelth *= 2;
                _field.DeleteElement(raspberries);

            }
        }
        public override ElementState GetState()
        {
            ElementState state = base.GetState();
            state.ImpactOnHelth = _impactOnHelth;
            return state;
        }
        public override Types GetElementType()
        {
            return Types.Bear;
        }
    }
}
