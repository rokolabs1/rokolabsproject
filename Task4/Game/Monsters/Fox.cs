﻿using System;
using Game.DTOs;

namespace Game
{
    public class Fox: Monster, IActor, IImpactOnHelth
    {
        public Fox(PlayingField field, int x, int y)
            : base(field, x, y)
        {
            Name = "Fox";
        }
        public Fox(PlayingField field, ElementState state)
            : base(field, state)
        {
            Name = "Fox";
        }
        public override int ImpactOnHelth { get; } = -10;
        public override void Draw()
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.Write("F");
        }
        public override Types GetElementType()
        {
            return Types.Fox;
        }
    }
}
