﻿using System;
using System.IO;
using Game.DTOs;
using Microsoft.Extensions.Configuration;

namespace Game
{
    class Program
    {
        static public void GetInsruction()
        {
            Console.WriteLine(
            $"Keys for the command:{Environment.NewLine}" +
            $"l - game leaderboard statistics; {Environment.NewLine}" +
            $"r - restoring the state of the game; {Environment.NewLine}" +
            $"s - save game state.{Environment.NewLine}");
        }
        static public void SelectMode(string userInput, string name)
        {
            if (userInput == "lg")
            {
                IMode mode = new LocalMode();
                Engine engine = new Engine(mode, name);
                engine.Run();
            }
            else if (userInput == "sg")
            {
                var builder = new ConfigurationBuilder()
                    .SetBasePath(Directory.GetCurrentDirectory())
                    .AddJsonFile("appsettings.json", optional: false);

                IConfiguration config = builder.Build();

                var connectionString = config.GetConnectionString("default");

                IMode mode = new DataAcssesLayer(connectionString);
                Engine engine = new Engine(mode, name);
                engine.Run();
            }
            else
            {
                Console.WriteLine("Wrong key.");
            }
        }
        static void Main(string[] args)
        {
            GetInsruction();
            Console.WriteLine("Enter player name:");
            string name = Console.ReadLine();
            while (String.IsNullOrWhiteSpace(name))
            {
                Console.WriteLine("Invalid name! Try again.");
                name = Console.ReadLine();
            }
            Console.WriteLine("Please enter the key to select the game mode:  lg - \"local game\" mode, sg - \"server game\" mode");
            string userInput = Console.ReadLine();
            Console.Clear();
            SelectMode(userInput, name);
        }
    }
}
