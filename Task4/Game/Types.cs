﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Game
{
    public enum Types
	{
        Bear = 1,
        Wolf = 2,
	    Fox = 3,
	    Bush = 4,
	    Stone = 5,
	    Wall = 6,
	    Apple = 7,
	    Pineapple = 8,
	    Blueberry = 9,
	    Raspberries = 10,
		Player = 11
    }
}
