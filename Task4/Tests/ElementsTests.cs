using Game;
using Game.DTOs;
using NUnit.Framework;

namespace Tests
{
    public class ElementsTests
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void PlayerTest()
        {
            Player player = new Player(null, null, 5, 10);
            Assert.AreEqual(5, player.CoordinateX);
            Assert.AreEqual(10, player.CoordinateY);
            Assert.IsTrue(player.IsAlive());
        }

        [Test]
        public void PlayerGetStateTest()
        {
            Player player = new Player(null, null, 5, 10);
            ElementState state = player.GetState();
            Assert.AreEqual(5, state.X);
            Assert.AreEqual(10, state.Y);
            Assert.AreEqual(100, state.Health);
            Assert.AreEqual(0, state.NumberOfPassesThroughObstacles);
            Assert.AreEqual(Types.Player, state.Type);
        }

        [Test]
        public void PlayerRestoreFromStateTest()
        {
            ElementState state = new ElementState(Types.Player, 15, 25)
            {
                Health = 1,
            };
            Player player = new Player(null, state, null);
            Assert.AreEqual(15, player.CoordinateX);
            Assert.AreEqual(25, player.CoordinateY);
            Assert.IsTrue(player.IsAlive());
            state.Health = 0;
            player = new Player(null, state, null);
            Assert.IsFalse(player.IsAlive());
        }

        [Test]
        public void BearTests()
        {
            Bear bear = new Bear(null, 1, 2);
            Assert.AreEqual(1, bear.CoordinateX);
            Assert.AreEqual(2, bear.CoordinateY);
            Assert.AreEqual(-30, bear.ImpactOnHelth);
        }

        [Test]
        public void GetBearStateTest()
        {
            Bear bear = new Bear(null, 1, 2);
            ElementState state = bear.GetState();
            Assert.AreEqual(1, state.X);
            Assert.AreEqual(2, state.Y);
            Assert.AreEqual(-30, state.ImpactOnHelth);
            Assert.AreEqual(Types.Bear, state.Type);
            Assert.AreEqual(0, state.NumberOfPassesThroughObstacles);
        }

        [Test]
        public void BearRestoreFromStateTest()
        {
            ElementState state = new ElementState(Types.Bear, 2, 3)
            {
                ImpactOnHelth = -15,
                NumberOfPassesThroughObstacles = 10
            };
            Bear bear = new Bear(null, state);
            Assert.AreEqual(2, bear.CoordinateX);
            Assert.AreEqual(3, bear.CoordinateY);
            Assert.AreEqual(-15, bear.ImpactOnHelth);
            Assert.AreEqual(10, bear.GetState().NumberOfPassesThroughObstacles);
        }
    }
}